﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STG.Animation
{
    static class Temp
    {   
        public static void Linear(ref float v, float amount)
        {
            v += amount;
        }

        public static void Linear(ref int v, int amount)
        {
            v += amount;
        }
    }
}
