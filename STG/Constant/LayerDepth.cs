﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STG.Constant
{
    internal static class LayerDepth
    {
        internal static float Entity => 0.7f;

        internal static float Particle => 0.1f;
        internal static float SideBar => 0.95f;
        internal static float SideBarText => 0.99f;
    }
}
