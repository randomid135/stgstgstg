﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STG.Constant
{
    internal static class Screen
    {
        internal static Vector2 ScreenBorderStart => new Vector2(260, 0);
        internal static Vector2 ScreenBorderEnd => new Vector2(Main.ScreenSize.X - 260, Main.ScreenSize.Y);

        internal static Vector2 BoundaryStart => new Vector2(160, -180);
        internal static Vector2 BoundaryEnd => new Vector2(Main.ScreenSize.X - 160, Main.ScreenSize.Y + 180);

        internal static Vector2 StrictBoundaryStart => new Vector2(40, -20);
        internal static Vector2 StrictBoundaryEnd => new Vector2(Main.ScreenSize.X - 40, Main.ScreenSize.Y + 20);
    }
}
