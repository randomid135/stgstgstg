﻿#if OPENGL
	#define SV_POSITION POSITION
	#define VS_SHADERMODEL vs_3_0
	#define PS_SHADERMODEL ps_3_0
#else
	#define VS_SHADERMODEL vs_4_0_level_9_1
	#define PS_SHADERMODEL ps_4_0_level_9_1
#endif

Texture2D SpriteTexture;
sampler BloomSampler : register(s0);
sampler OriginSampler : register(s1)
{
	Texture = (OriginTexture);
	Filter = Linear;
	AddressU = clamp;
	AddressV = clamp;
};

float BloomIntensity;
float OriginIntensity;
float BloomSaturation;
float OriginSaturation;

sampler2D SpriteTextureSampler = sampler_state
{
	Texture = <SpriteTexture>;
};

struct VertexShaderOutput
{
	float4 Position : SV_POSITION;
	float4 Color : COLOR0;
	float2 TextureCoordinates : TEXCOORD0;
};

float4 AdjustSaturation(float4 color, float saturation)
{
	float grey = dot(color, float3(0.3, 0.59, 0.11));

	return lerp(grey, color, saturation);
}

float4 MainPS(VertexShaderOutput input) : COLOR
{
	float4 bloom = tex2D(BloomSampler, input.TextureCoordinates);
	float4 origin = tex2D(OriginSampler, input.TextureCoordinates);

	bloom = AdjustSaturation(bloom, BloomSaturation) * BloomIntensity;
	origin = AdjustSaturation(origin, OriginSaturation);

	origin *= (1 - saturate(bloom));

	return bloom + origin;
}

technique SpriteDrawing
{
	pass P0
	{
		PixelShader = compile PS_SHADERMODEL MainPS();
	}
};