﻿using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STG.Content
{
    static class Audio
    {
        //public static Song Music { get; private set; }

        public static SoundEffect PlayerExplosion { get; private set; }

        public static SoundEffect PlayerBomb { get; private set; }

        public static SoundEffect PlayerShot { get; private set; }

        public static SoundEffect PlayerGainsLife { get; private set; }

        public static SoundEffect CommonExplosion { get; private set; }

        public static SoundEffect EnemyShot { get; private set; }

        public static void LoadContent(ContentManager content)
        {
            
            PlayerShot = content.Load<SoundEffect>("Asset/Audio/laser");
            PlayerBomb = content.Load<SoundEffect>("Asset/Audio/playerBomb");
            CommonExplosion = content.Load<SoundEffect>("Asset/Audio/explosion1");
            PlayerExplosion = content.Load<SoundEffect>("Asset/Audio/playerExplosion");
            PlayerGainsLife = content.Load<SoundEffect>("Asset/Audio/extend");
            EnemyShot = content.Load<SoundEffect>("Asset/Audio/shot");
        }
    }
}
