﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STG.Content
{
    class Background
    {
        private Texture2D Image;
        private Vector2 Position;
        public Vector2 Velocity;
        public float Scale;

        public Background(Texture2D image, Vector2 velocity, float scale = 1f)
        {
            Image = image;
            Position = new Vector2(640, 1280 - Image.Height);
            Velocity = velocity;
            Scale = scale;
        }

        public void Update()
        {
            if (Position.Y < Image.Height)
                Position += Velocity;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Image, Position, null, Color.White, 0, new Vector2(Image.Width / 2, 0), Scale, SpriteEffects.None, 0f);
        }
    }
}
