﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STG.Content.CustomEffect
{
    public class Bloom
    {
        GraphicsDevice graphics;
        SpriteBatch spriteBatch;
        RenderTarget2D sceneRenderTarget, finalRenderTarget, renderTarget0, renderTarget1;
        public RenderTarget2D FinalRenderTarget
        {
            get
            {
                return finalRenderTarget;
            }
        }

        public BloomSettings Settings { get; set; } = BloomSettings.PresetSettings[0];

        public enum IntermediateBuffer
        {
            PreBloom,
            BlurredHorizontally,
            BlurredBothWays,
            FinalResult
        }

        IntermediateBuffer showBuffer = IntermediateBuffer.FinalResult;

        public IntermediateBuffer ShowBuffer
        {
            get
            {
                return ShowBuffer;
            }
            set
            {
                showBuffer = value;
            }
        }

        public Bloom(GraphicsDevice graphics, SpriteBatch spriteBatch)
        {
            this.graphics = graphics;
            this.spriteBatch = spriteBatch;
        }

        public void LoadContent(PresentationParameters pp)
        {
            int width = pp.BackBufferWidth;
            int height = pp.BackBufferHeight;

            finalRenderTarget = new RenderTarget2D(graphics, width, height, false, SurfaceFormat.Color, pp.DepthStencilFormat, pp.MultiSampleCount, RenderTargetUsage.DiscardContents);
            sceneRenderTarget = new RenderTarget2D(graphics, width, height, false, SurfaceFormat.Color, pp.DepthStencilFormat, pp.MultiSampleCount, RenderTargetUsage.DiscardContents);
            renderTarget0 = new RenderTarget2D(graphics, width, height, false, SurfaceFormat.Color, DepthFormat.None);
            renderTarget1 = new RenderTarget2D(graphics, width, height, false, SurfaceFormat.Color, DepthFormat.None);
        }

        public void UnloadContent()
        {
            sceneRenderTarget.Dispose();
            renderTarget0.Dispose();
            renderTarget1.Dispose();
        }

        public void Begin()
        {
            graphics.SetRenderTarget(sceneRenderTarget);
            graphics.Clear(Color.TransparentBlack);
        }

        public void End()
        {
            graphics.SamplerStates[1] = SamplerState.LinearClamp;
            EffectSource.BloomExtract.Parameters["BloomThreshold"].SetValue(Settings.BloomThreshold);
            DrawFullScreenQuad(sceneRenderTarget, renderTarget0, EffectSource.BloomExtract, IntermediateBuffer.PreBloom);

            SetBlurParameters(1.0f / (float)renderTarget0.Width, 0);
            DrawFullScreenQuad(renderTarget0, renderTarget1, EffectSource.GaussianBlur, IntermediateBuffer.BlurredHorizontally);

            SetBlurParameters(0, 1.0f / (float)renderTarget0.Height);
            DrawFullScreenQuad(renderTarget1, renderTarget0, EffectSource.GaussianBlur, IntermediateBuffer.BlurredBothWays);

            graphics.SetRenderTarget(finalRenderTarget);

            EffectParameterCollection parameters = EffectSource.Bloom.Parameters;

            parameters["BloomIntensity"].SetValue(Settings.BloomIntensity);
            parameters["OriginIntensity"].SetValue(Settings.OriginIntensity);
            parameters["BloomSaturation"].SetValue(Settings.BloomSaturation);
            parameters["OriginSaturation"].SetValue(Settings.OriginSaturation);

            EffectSource.Bloom.Parameters["OriginTexture"].SetValue(sceneRenderTarget);

            Viewport viewport = graphics.Viewport;

            DrawFullScreenQuad(renderTarget0, viewport.Width, viewport.Height, EffectSource.Bloom, IntermediateBuffer.FinalResult);

            graphics.SetRenderTarget(null);
        }

        void DrawFullScreenQuad(Texture2D texture, RenderTarget2D renderTarget, Effect effect, IntermediateBuffer currentBuffer)
        {
            graphics.SetRenderTarget(renderTarget);
            DrawFullScreenQuad(texture, renderTarget.Width, renderTarget.Height, effect, currentBuffer);
        }

        void DrawFullScreenQuad(Texture2D texture, int width, int height, Effect effect, IntermediateBuffer currentBuffer)
        {
            if (showBuffer < currentBuffer)
                effect = null;
            graphics.Clear(Color.TransparentBlack);
            spriteBatch.Begin(0, BlendState.AlphaBlend, null, null, null, effect);
            spriteBatch.Draw(texture, new Rectangle(0, 0, width, height), Color.White);
            spriteBatch.End();
        }

        void SetBlurParameters(float dx, float dy)
        {
            EffectParameter weightsParameter, offsetsParameter;
            weightsParameter = EffectSource.GaussianBlur.Parameters["SampleWeights"];
            offsetsParameter = EffectSource.GaussianBlur.Parameters["SampleOffsets"];

            int sampleCount = weightsParameter.Elements.Count;

            float[] sampleWeights = new float[sampleCount];
            Vector2[] sampleOffsets = new Vector2[sampleCount];

            sampleWeights[0] = ComputeGaussian(0);
            sampleOffsets[0] = Vector2.Zero;

            float totalWeights = sampleWeights[0];

            for (int i = 0; i < sampleCount / 2; ++i)
            {
                float weight = ComputeGaussian(i + 1);

                sampleWeights[i * 2 + 1] = weight;
                sampleWeights[i * 2 + 2] = weight;

                totalWeights += weight * 2;

                float sampleOffset = i * 2 + 1.5f;

                Vector2 delta = new Vector2(dx, dy) * sampleOffset;

                sampleOffsets[i * 2 + 1] = delta;
                sampleOffsets[i * 2 + 2] = -delta;
            }

            for (int i = 0; i < sampleWeights.Length; ++i)
                sampleWeights[i] /= totalWeights;

            weightsParameter.SetValue(sampleWeights);
            offsetsParameter.SetValue(sampleOffsets);
        }

        float ComputeGaussian(float n)
        {
            float theta = Settings.BlurAmount;
            return (float)(((1.0) / Math.Sqrt(2 * Math.PI * theta)) * Math.Exp(-(n * n) / (2 * theta * theta)));
        }
    }
}
