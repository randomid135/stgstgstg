﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STG.Content.CustomEffect
{
    public class BloomSettings
    {
        public string Name;

        public float BloomThreshold;

        public float BlurAmount;

        public float BloomIntensity;
        public float OriginIntensity;

        public float BloomSaturation;
        public float OriginSaturation;

        public BloomSettings(string name, float bloomThreshold, float blurAmount, float bloomIntensity, float originIntensity, float bloomSaturation, float originSaturation)
        {
            Name = name;
            BloomThreshold = bloomThreshold;
            BlurAmount = blurAmount;
            BloomIntensity = bloomIntensity;
            OriginIntensity = originIntensity;
            BloomSaturation = bloomSaturation;
            OriginSaturation = originSaturation;
        }

        public static BloomSettings[] PresetSettings =
        {
            new BloomSettings("Default", 0, 3, 1f, 1, 1, 1),
            new BloomSettings("Soft", 0, 3, 1, 1, 1, 1),
            new BloomSettings("Blurry", 0, 2.5f, 4f, 0, 1, 1)
            //new BloomSettings("Blurry", 0, 2, 1, 0.1f, 1, 1)
        };
    }
}
