﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STG.Content
{
    static class EffectSource
    {
        public static Effect BloomExtract { get; private set; }
        public static Effect GaussianBlur { get; private set; }
        public static Effect Bloom { get; private set; }

        public static void LoadContent(ContentManager content)
        {
            BloomExtract = content.Load<Effect>("Asset/Effect/BloomExtract");
            GaussianBlur = content.Load<Effect>("Asset/Effect/GaussianBlur");
            Bloom = content.Load<Effect>("Asset/Effect/Bloom");
        }
    }
}
