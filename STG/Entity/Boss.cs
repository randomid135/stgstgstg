﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STG.Entity
{
    partial class Boss : Enemy
    {
        bool IsLast;
        Action<Vector2> NextPattern;
        UI.HealthBar HealthBar;

        public static void SubManager(Vector2 position)
        {
            var boss = new Boss(Content.Sprite.Boss1, position, 30);
            //boss.AddMovement(boss.MoveInASquare());
            boss.AddShootingPattern(boss.Shoot(LinearStaticRandomWays, Content.Sprite.EllipseBullet_B, false, new object[] { 5f, 32 }, 25, 200, 50));
            boss.AddShootingPattern(boss.Shoot(LinearStaticRandomWays, Content.Sprite.EllipseBullet_Y, false, new object[] { 7f, 48 }, 10, 200, 50));
            boss.SetNextPattern(SubManagerPattern1);

            Manager.Add(boss);
        }

        protected static void SubManagerPattern1(Vector2 position)
        {
            var boss = new Boss(Content.Sprite.Boss1, position, 3000);
            //boss.AddMovement(boss.MoveSideToSide());
            boss.AddShootingPattern(boss.Shoot(LinearRandom, Content.Sprite.EllipseBullet_Y, false, new object[] { 5f }, 1, 200, 50));
            boss.AddShootingPattern(boss.Shoot(LinearRandom, Content.Sprite.EllipseBullet_G, false, new object[] { 6f }, 1, 200, 50));
            boss.AddShootingPattern(boss.Shoot(LinearRandom, Content.Sprite.EllipseBullet_R, false, new object[] { 4f }, 1, 200, 50));
            boss.AddShootingPattern(boss.Shoot(LinearRandom, Content.Sprite.EllipseBullet_Y, false, new object[] { 5f }, 1, 200, 50));
            boss.AddShootingPattern(boss.Shoot(LinearRandom, Content.Sprite.EllipseBullet_G, false, new object[] { 6f }, 1, 200, 50));
            boss.AddShootingPattern(boss.Shoot(LinearRandom, Content.Sprite.EllipseBullet_R, false, new object[] { 4f }, 1, 200, 50));

            Manager.Add(boss);
        }
    }
    partial class Boss
    {
        public Boss(Texture2D image, Vector2 position, int hp, bool isLast = false)
        {
            this.image = image;
            Position = position;
            Radius = image.Width / 2.5f;
            color = Color.White;
            HP = hp;
            IsLast = isLast;
            HealthBar = new UI.HealthBar(HP);
        }

        public void SetNextPattern(Action<Vector2> nextPattern)
        {
            NextPattern = nextPattern;
        }

        public override void Update(GameTime gameTime)
        {
            HealthBar.CurrentHealth = HP;
            base.Update(gameTime);
        }

        public override void Kill()
        {
            NextPattern(Position);
            Manager.ClearBullets();
            base.Kill();
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
            HealthBar.Draw(spriteBatch);
        }
    }
}
