﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STG.Entity
{
    partial class CommonEnemy : Enemy
    {
        public static void Stage1Type1(Vector2 position)
        {
            var enemy = new CommonEnemy(Content.Sprite.Enemy1, position, 12, 1000);

            enemy.AddShootingPattern(enemy.Shoot(LinearStaticWays, Content.Sprite.EllipseBullet_B, false, new object[] { 6f, 0f, 8 }, 40, 1000, 50));
            enemy.AddMovement(enemy.MoveInAngle(0f, 2f, 1f));

            Manager.Add(enemy);
        }

        public static void Stage1Type1_2(Vector2 position)
        {
            var enemy = new CommonEnemy(Content.Sprite.Enemy1, position, 12, 1000);

            enemy.AddShootingPattern(enemy.Shoot(LinearStaticWays, Content.Sprite.EllipseBullet_R, false, new object[] { 6f, 22.5f, 8 }, 40, 1000, 50));
            enemy.AddMovement(enemy.MoveInAngle(180f, 2f, 1f));

            Manager.Add(enemy);
        }

        public static void Stage1Type2(Vector2 position)
        {
            var enemy = new CommonEnemy(Content.Sprite.Enemy1, position, 12, 1000);

            enemy.AddShootingPattern(enemy.Shoot(SeekingStaticWays, Content.Sprite.EllipseBullet_Y, false, new object[] { 10f, 0f, 24 }, 20, 1000, 50));
            enemy.AddMovement(enemy.MoveInAngle(20f, 6f, 1f));

            Manager.Add(enemy);
        }

        public static void Stage1Type2_2(Vector2 position)
        {
            var enemy = new CommonEnemy(Content.Sprite.Enemy1, position, 12, 1000);

            enemy.AddShootingPattern(enemy.Shoot(SeekingStaticWays, Content.Sprite.EllipseBullet_Y, false, new object[] { 10f, 0f, 24 }, 20, 1000, 50));
            enemy.AddMovement(enemy.MoveInAngle(160f, 6f, 1f));

            Manager.Add(enemy);
        }

        public static void Stage1Type3(Vector2 position)
        {
            var enemy = new CommonEnemy(Content.Sprite.Enemy1, position, 320, 1000);

            enemy.AddShootingPattern(enemy.Shoot(LinearRandom, Content.Sprite.EllipseBullet_G, false, new object[] { 3f }, 8, 1000, 50));
            enemy.AddShootingPattern(enemy.Shoot(LinearRandom, Content.Sprite.EllipseBullet_Y, false, new object[] { 3f }, 8, 1000, 50));
            enemy.AddMovement(enemy.MoveInAngle(90f, 6f, 0.98f));

            Manager.Add(enemy);
        }

        public static void Stage1Type3_2(Vector2 position)
        {
            var enemy = new CommonEnemy(Content.Sprite.Enemy1, position, 200, 1000);

            enemy.AddShootingPattern(enemy.Shoot(LinearRandom, Content.Sprite.EllipseBullet_R, false, new object[] { 5f }, 8, 1000, 50));
            enemy.AddShootingPattern(enemy.Shoot(LinearRandom, Content.Sprite.EllipseBullet_B, false, new object[] { 5f }, 8, 1000, 50));
            enemy.AddMovement(enemy.MoveInAngle(90f, 9f, 0.98f));

            Manager.Add(enemy);
        }

        public static void Stage1Type4(Vector2 position)
        {
            var enemy = new CommonEnemy(Content.Sprite.Enemy1, position, 120, 1000);

            enemy.AddShootingPattern(enemy.Shoot(SeekingWithVariance, Content.Sprite.MediumBullet_V, false, new object[] { 6f, 8f }, 2, 10, 40));

            //enemy.AddShootingPattern(enemy.Shoot(LinearStaticWays, Content.Sprite.EllipseBullet_W, false, new object[] { 5f, 5f, 17 }, 50, 1000, 50));
            //enemy.AddShootingPattern(enemy.Shoot(SeekingWithVariance, Content.Sprite.MediumBullet_V, false, new object[] { 12f, 20f }, 4, 25, 50));
            //enemy.AddOnDeathPattern(enemy.Shoot(LinearRandomWays, Content.Sprite.MediumBullet_Y, false, new object[] { 16f, 72 }, 1, 1000, 50));
            //enemy.AddOnDeathPattern(enemy.Shoot(LinearRandomWays, Content.Sprite.MediumBullet_G, false, new object[] { 1f, 144 }, 4, 1000, 50));
            enemy.AddMovement(enemy.MoveInAngle(90f, 6f, 0.98f));

            Manager.Add(enemy);
        }

        public static void Stage1Type4_2(Vector2 position)
        {
            var enemy = new CommonEnemy(Content.Sprite.Enemy1, position, 100, 1000);

            enemy.AddShootingPattern(enemy.Shoot(SeekingWithVariance, Content.Sprite.MediumBullet_V, false, new object[] { 8f, 8f }, 2, 16, 40));
            enemy.AddMovement(enemy.MoveInAngle(90f, 6f, 0.98f));

            Manager.Add(enemy);
        }

        public static void Stage1Type4_3(Vector2 position)
        {
            var enemy = new CommonEnemy(Content.Sprite.Enemy1, position, 80, 1000);

            enemy.AddShootingPattern(enemy.Shoot(SeekingWithVariance, Content.Sprite.MediumBullet_V, false, new object[] { 12f, 12f }, 2, 20, 40));

            enemy.AddMovement(enemy.MoveInAngle(90f, 6f, 0.98f));

            Manager.Add(enemy);
        }

        public static void Stage1Type5(Vector2 position)
        {
            var enemy = new CommonEnemy(Content.Sprite.Enemy1, position, 8, 1000);

            enemy.AddShootingPattern(enemy.Shoot(LinearRandom, Content.Sprite.EllipseBullet_W, false, new object[] { 8f }, 4, 1000, 50));
            enemy.AddShootingPattern(enemy.Shoot(LinearRandom, Content.Sprite.EllipseBullet_B, false, new object[] { 1f }, 4, 1000, 50));
           
            enemy.AddMovement(enemy.MoveInAngleAlt(0f, 1f, 1.02f));
            enemy.AddMovement(enemy.MoveInAngleAlt(90f, 1.5f, 1f));

            Manager.Add(enemy);
        }

        public static void Stage1Type6(Vector2 position)
        {
            var enemy = new CommonEnemy(Content.Sprite.Enemy1, position, 8, 1000);

            enemy.AddShootingPattern(enemy.Shoot(LinearRandom, Content.Sprite.EllipseBullet_W, false, new object[] { 8f }, 4, 1000, 50));
            enemy.AddShootingPattern(enemy.Shoot(LinearRandom, Content.Sprite.EllipseBullet_R, false, new object[] { 1f }, 4, 1000, 50));

            enemy.AddMovement(enemy.MoveInAngleAlt(180f, 1f, 1.02f));
            enemy.AddMovement(enemy.MoveInAngleAlt(90f, 1.5f, 1f));

            Manager.Add(enemy);
        }

        public static void Stage1Type7(Vector2 position)
        {
            var enemy = new CommonEnemy(Content.Sprite.Enemy1, position, 200, 1000);

            enemy.AddShootingPattern(enemy.Shoot(LinearDynamicWays, Content.Sprite.MediumBullet_R, false, new object[] { 4f, 0f, 8, 4f }, 25, 150, 50));

            enemy.AddMovement(enemy.MoveInAngle(90f, 3.5f, 0.99f));

            Manager.Add(enemy);
        }

        public static void Stage1Type8(Vector2 position)
        {
            var enemy = new CommonEnemy(Content.Sprite.Enemy1, position, 500, 1000);

            enemy.AddShootingPattern(enemy.Shoot(LinearDynamicWays, Content.Sprite.MediumBullet_B, false, new object[] { 3f, 0f, 6, 4f }, 4, 20, 50));

            enemy.AddMovement(enemy.MoveInAngle(90f, 4f, 0.99f));

            Manager.Add(enemy);
        }

        public static void Stage1Type8_2(Vector2 position)
        {
            var enemy = new CommonEnemy(Content.Sprite.Enemy1, position, 500, 1000);

            enemy.AddShootingPattern(enemy.Shoot(LinearDynamicWays, Content.Sprite.MediumBullet_B, false, new object[] { 3f, 90f, 6, -4f }, 4, 20, 50));

            enemy.AddMovement(enemy.MoveInAngle(90f, 4f, 0.99f));

            Manager.Add(enemy);
        }

        public static void Stage1Type9(Vector2 position)
        {
            var enemy = new CommonEnemy(Content.Sprite.Enemy1, position, 8, 1000);

            enemy.AddShootingPattern(enemy.Shoot(Seeking, Content.Sprite.EllipseBullet_G, false, new object[] { 12f }, 1, 8, 20));

            enemy.AddMovement(enemy.MoveInAngle(30f, 2f, 1.01f));

            Manager.Add(enemy);
        }

        public static void Stage1Type9_2(Vector2 position)
        {
            var enemy = new CommonEnemy(Content.Sprite.Enemy1, position, 8, 1000);

            enemy.AddShootingPattern(enemy.Shoot(Seeking, Content.Sprite.EllipseBullet_G, false, new object[] { 12f }, 1, 8, 20));

            enemy.AddMovement(enemy.MoveInAngle(150f, 2f, 1.01f));

            Manager.Add(enemy);
        }

        public static void Stage1Type10(Vector2 position)
        {
            var enemy = new CommonEnemy(Content.Sprite.Enemy1, position, 8, 1000);

            enemy.AddShootingPattern(enemy.Shoot(Seeking, Content.Sprite.EllipseBullet_G, false, new object[] { 12f }, 1, 8, 16));

            enemy.AddMovement(enemy.MoveInAngle(20f, 3f, 1.015f));

            Manager.Add(enemy);
        }

        public static void Stage1Type10_2(Vector2 position)
        {
            var enemy = new CommonEnemy(Content.Sprite.Enemy1, position, 8, 1000);

            enemy.AddShootingPattern(enemy.Shoot(Seeking, Content.Sprite.EllipseBullet_G, false, new object[] { 12f }, 1, 8, 16));

            enemy.AddMovement(enemy.MoveInAngle(160f, 3f, 1.015f));

            Manager.Add(enemy);
        }

        public static void Stage1Type11(Vector2 position)
        {
            var enemy = new CommonEnemy(Content.Sprite.Enemy1, position, 20, 1000);

            enemy.AddShootingPattern(enemy.Shoot(LinearRandom, Content.Sprite.EllipseBullet_W, false, new object[] { 8f }, 2, 500, 16));
            enemy.AddShootingPattern(enemy.Shoot(LinearRandom, Content.Sprite.MediumBullet_Y, false, new object[] { 2f }, 2, 500, 16));

            enemy.AddMovement(enemy.MoveInAngle(90f, 2f, 1.02f));

            Manager.Add(enemy);
        }

        public static void Stage1Type11_2(Vector2 position)
        {
            var enemy = new CommonEnemy(Content.Sprite.Enemy1, position, 20, 1000);

            enemy.AddShootingPattern(enemy.Shoot(LinearRandom, Content.Sprite.EllipseBullet_W, false, new object[] { 8f }, 2, 500, 16));
            enemy.AddShootingPattern(enemy.Shoot(LinearRandom, Content.Sprite.MediumBullet_G, false, new object[] { 2f }, 2, 500, 16));

            enemy.AddMovement(enemy.MoveInAngle(270f, 2f, 1.02f));

            Manager.Add(enemy);
        }

        public static void Stage1Type12(Vector2 position)
        {
            var enemy = new CommonEnemy(Content.Sprite.Enemy1, position, 400, 1000);

            enemy.AddShootingPattern(enemy.Shoot(LinearDynamicWays, Content.Sprite.MediumBullet_B, false, new object[] { 4f, 45f, 6, 3f }, 2, 8, 24));
            enemy.AddShootingPattern(enemy.Shoot(LinearDynamicWays, Content.Sprite.MediumBullet_R, false, new object[] { 2f, -45f, 6, 1.5f }, 1, 5, 50));

            enemy.AddMovement(enemy.MoveInAngle(45f, 6f, 0.97f));

            Manager.Add(enemy);
        }

        public static void Stage1Type12_2(Vector2 position)
        {
            var enemy = new CommonEnemy(Content.Sprite.Enemy1, position, 300, 1000);

            enemy.AddShootingPattern(enemy.Shoot(LinearDynamicWays, Content.Sprite.MediumBullet_B, false, new object[] { 4f, 45f, 6, -3f }, 4, 20, 50));
            enemy.AddShootingPattern(enemy.Shoot(LinearDynamicWays, Content.Sprite.MediumBullet_R, false, new object[] { 2f, -45f, 6, -1.5f }, 1, 5, 50));

            enemy.AddMovement(enemy.MoveInAngle(135f, 6f, 0.97f));

            Manager.Add(enemy);
        }

        public static void Stage1Type13(Vector2 position)
        {
            var enemy = new CommonEnemy(Content.Sprite.Enemy1, position, 30, 1500);

            enemy.AddShootingPattern(enemy.Shoot(LinearStaticRandomWays, Content.Sprite.EllipseBullet_R, false, new object[] { 4f, 16 }, 120, 2000, 100));

            enemy.AddMovement(enemy.MoveInAngleAlt(90f, .6f, 0.97f));
            enemy.AddMovement(enemy.MoveInAngleAlt(0f, .05f, MathUtility.Rng.NextFloat(1.0015f, 1.0025f)));

            Manager.Add(enemy);
        }

        public static void Stage1Type13_2(Vector2 position)
        {
            var enemy = new CommonEnemy(Content.Sprite.Enemy1, position, 30, 1500);

            enemy.AddShootingPattern(enemy.Shoot(LinearStaticRandomWays, Content.Sprite.EllipseBullet_R, false, new object[] { 2f, 16 }, 120, 2000, 100));

            enemy.AddMovement(enemy.MoveInAngleAlt(90f, .8f, 0.97f));
            enemy.AddMovement(enemy.MoveInAngleAlt(180f, .05f, MathUtility.Rng.NextFloat(1.0015f, 1.0025f)));

            Manager.Add(enemy);
        }

        public static void Stage1Type13_3(Vector2 position)
        {
            var enemy = new CommonEnemy(Content.Sprite.Enemy1, position, 30, 1500);

            enemy.AddShootingPattern(enemy.Shoot(LinearStaticRandomWays, Content.Sprite.EllipseBullet_W, false, new object[] { 6f, 16 }, 120, 2000, 100));

            enemy.AddMovement(enemy.MoveInAngle(MathUtility.Rng.NextFloat(60f, 120f), 7f, 1f));

            Manager.Add(enemy);
        }

        public static void Stage1Type14(Vector2 position)
        {
            var enemy = new CommonEnemy(Content.Sprite.Enemy1, position, 16, 1500);

            enemy.AddShootingPattern(enemy.Shoot(AcceleratingSeekingWithVariance, Content.Sprite.MediumBullet_V, false, new object[] { 16f, 20f, 0.97f, 2.5f, 8f }, 1, 8, 80));
            enemy.AddMovement(enemy.MoveInAngle(0f, 3f, 1f));

            Manager.Add(enemy);
        }

        public static void Stage1Type14_2(Vector2 position)
        {
            var enemy = new CommonEnemy(Content.Sprite.Enemy1, position, 16, 1500);

            enemy.AddShootingPattern(enemy.Shoot(AcceleratingSeekingWithVariance, Content.Sprite.MediumBullet_V, false, new object[] { 16f, 20f, 0.97f, 2.5f, 8f }, 1, 8, 80));
            enemy.AddMovement(enemy.MoveInAngle(180f, 3f, 1f));

            Manager.Add(enemy);
        }

        public static void Stage1Type15(Vector2 position)
        {
            var enemy = new CommonEnemy(Content.Sprite.Enemy3, position, 8, 2000);

            enemy.AddShootingPattern(enemy.Shoot(AcceleratingInRange, Content.Sprite.EllipseBullet_B, false, new object[] { 2f, 85f, 95f, 1.03f }, 8, 32, 0));
            enemy.AddShootingPattern(enemy.Shoot(AcceleratingInRange, Content.Sprite.EllipseBullet_B, false, new object[] { 2f, 265f, 275f, 1.03f }, 8, 32, 0));
            enemy.AddShootingPattern(enemy.Shoot(AcceleratingInRange, Content.Sprite.EllipseBullet_B, false, new object[] { 3f, 85f, 95f, 1.03f }, 8, 32, 0));
            enemy.AddShootingPattern(enemy.Shoot(AcceleratingInRange, Content.Sprite.EllipseBullet_B, false, new object[] { 3f, 265f, 275f, 1.03f }, 8, 32, 0));

            enemy.AddOnDeathPattern(enemy.Shoot(LinearRandomWays, Content.Sprite.MediumBullet_R, false, new object[] { 6f, 72 }, 4, 1000, 50));
            enemy.AddMovement(enemy.MoveInAngle(5f, 7f, 1f));

            Manager.Add(enemy);
        }

        public static void Stage1Type16(Vector2 position)
        {
            var enemy = new CommonEnemy(Content.Sprite.Enemy1, position, 6, 2000);

            enemy.AddShootingPattern(enemy.Shoot(LinearRandom, Content.Sprite.EllipseBullet_W, false, new object[] { 8f }, 2, 1000, 50));
            enemy.AddShootingPattern(enemy.Shoot(LinearRandom, Content.Sprite.EllipseBullet_B, false, new object[] { 1f }, 2, 1000, 50));

            enemy.AddMovement(enemy.MoveInAngleAlt(0f, 1f, 1.02f));
            enemy.AddMovement(enemy.MoveInAngleAlt(90f, 1.5f, 1f));

            Manager.Add(enemy);
        }

        public static void Stage1Type16_2(Vector2 position)
        {
            var enemy = new CommonEnemy(Content.Sprite.Enemy1, position, 6, 2000);

            enemy.AddShootingPattern(enemy.Shoot(LinearRandom, Content.Sprite.EllipseBullet_W, false, new object[] { 8f }, 2, 1000, 50));
            enemy.AddShootingPattern(enemy.Shoot(LinearRandom, Content.Sprite.EllipseBullet_R, false, new object[] { 1f }, 2, 1000, 50));

            enemy.AddMovement(enemy.MoveInAngleAlt(180f, 1f, 1.02f));
            enemy.AddMovement(enemy.MoveInAngleAlt(90f, 1.5f, 1f));

            Manager.Add(enemy);
        }

        public static void Stage1Type16_3(Vector2 position)
        {
            var enemy = new CommonEnemy(Content.Sprite.Enemy1, position, 6, 2000);

            enemy.AddShootingPattern(enemy.Shoot(LinearRandom, Content.Sprite.EllipseBullet_W, false, new object[] { 8f }, 2, 1000, 50));
            enemy.AddShootingPattern(enemy.Shoot(LinearRandom, Content.Sprite.EllipseBullet_R, false, new object[] { 1f }, 2, 1000, 50));

            enemy.AddMovement(enemy.MoveInAngleAlt(0f, 1f, 1.02f));
            enemy.AddMovement(enemy.MoveInAngleAlt(270f, 1.2f, 1f));

            Manager.Add(enemy);
        }

        public static void Stage1Type16_4(Vector2 position)
        {
            var enemy = new CommonEnemy(Content.Sprite.Enemy1, position, 6, 2000);

            enemy.AddShootingPattern(enemy.Shoot(LinearRandom, Content.Sprite.EllipseBullet_W, false, new object[] { 8f }, 2, 1000, 50));
            enemy.AddShootingPattern(enemy.Shoot(LinearRandom, Content.Sprite.EllipseBullet_R, false, new object[] { 1f }, 2, 1000, 50));

            enemy.AddMovement(enemy.MoveInAngleAlt(180f, 1f, 1.02f));
            enemy.AddMovement(enemy.MoveInAngleAlt(270f, 1.2f, 1f));

            Manager.Add(enemy);
        }

        public static void Stage1Type17(Vector2 position)
        {
            var enemy = new CommonEnemy(Content.Sprite.Enemy3, position, 4, 3000);

            enemy.AddShootingPattern(enemy.Shoot(Seeking, Content.Sprite.EllipseBullet_W, false, new object[] { 10f }, 2, 8, 32));

            enemy.AddOnDeathPattern(enemy.Shoot(LinearRandomWays, Content.Sprite.MediumBullet_Y, false, new object[] { 8f, 48 }, 1, 1000, 50));
            enemy.AddMovement(enemy.MoveInAngle(175f, 6f, 1f));

            Manager.Add(enemy);
        }

        public static void Stage1Type17_2(Vector2 position)
        {
            var enemy = new CommonEnemy(Content.Sprite.Enemy3, position, 4, 3000);

            enemy.AddShootingPattern(enemy.Shoot(Seeking, Content.Sprite.EllipseBullet_W, false, new object[] { 10f }, 2, 8, 32));

            enemy.AddOnDeathPattern(enemy.Shoot(LinearRandomWays, Content.Sprite.MediumBullet_G, false, new object[] { 8f, 48 }, 1, 1000, 50));
            enemy.AddMovement(enemy.MoveInAngle(5f, 6f, 1f));

            Manager.Add(enemy);
        }

        public static void Stage1Type17_3(Vector2 position)
        {
            var enemy = new CommonEnemy(Content.Sprite.Enemy3, position, 4, 3000);

            enemy.AddShootingPattern(enemy.Shoot(Seeking, Content.Sprite.EllipseBullet_W, false, new object[] { 10f }, 1, 6, 18));

            enemy.AddOnDeathPattern(enemy.Shoot(LinearRandomWays, Content.Sprite.MediumBullet_Y, false, new object[] { 5f, 36 }, 1, 1000, 50));
            enemy.AddMovement(enemy.MoveInAngle(175f, 12f, 1f));

            Manager.Add(enemy);
        }

        public static void Stage1Type17_4(Vector2 position)
        {
            var enemy = new CommonEnemy(Content.Sprite.Enemy3, position, 4, 3000);

            enemy.AddShootingPattern(enemy.Shoot(Seeking, Content.Sprite.EllipseBullet_W, false, new object[] { 10f }, 1, 6, 18));

            enemy.AddOnDeathPattern(enemy.Shoot(LinearRandomWays, Content.Sprite.MediumBullet_G, false, new object[] { 5f, 36 }, 1, 1000, 50));
            enemy.AddMovement(enemy.MoveInAngle(5f, 12f, 1f));

            Manager.Add(enemy);
        }

        public static void Stage1Type17_5(Vector2 position)
        {
            var enemy = new CommonEnemy(Content.Sprite.Enemy3, position, 4, 3000);

            enemy.AddShootingPattern(enemy.Shoot(Seeking, Content.Sprite.EllipseBullet_W, false, new object[] { 10f }, 1, 6, 18));

            enemy.AddOnDeathPattern(enemy.Shoot(LinearRandomWays, Content.Sprite.MediumBullet_Y, false, new object[] { 8f, 28 }, 1, 1000, 50));
            enemy.AddMovement(enemy.MoveInAngle(170f, 16f, 1f));

            Manager.Add(enemy);
        }

        public static void Stage1Type17_6(Vector2 position)
        {
            var enemy = new CommonEnemy(Content.Sprite.Enemy3, position, 4, 3000);

            enemy.AddShootingPattern(enemy.Shoot(Seeking, Content.Sprite.EllipseBullet_W, false, new object[] { 10f }, 1, 6, 18));

            enemy.AddOnDeathPattern(enemy.Shoot(LinearRandomWays, Content.Sprite.MediumBullet_G, false, new object[] { 8f, 28 }, 1, 1000, 50));
            enemy.AddMovement(enemy.MoveInAngle(10f, 16f, 1f));

            Manager.Add(enemy);
        }

        public static void Stage1Type17_7(Vector2 position)
        {
            var enemy = new CommonEnemy(Content.Sprite.Enemy3, position, 4, 3000);

            enemy.AddShootingPattern(enemy.Shoot(Seeking, Content.Sprite.EllipseBullet_W, false, new object[] { 10f }, 1, 6, 18));

            enemy.AddOnDeathPattern(enemy.Shoot(LinearRandomWays, Content.Sprite.MediumBullet_B, false, new object[] { 2f, 48 }, 1, 1000, 50));
            enemy.AddMovement(enemy.MoveInAngle(170f, 20f, 1f));

            Manager.Add(enemy);
        }

        public static void Stage1Type17_8(Vector2 position)
        {
            var enemy = new CommonEnemy(Content.Sprite.Enemy3, position, 4, 3000);

            enemy.AddShootingPattern(enemy.Shoot(Seeking, Content.Sprite.EllipseBullet_W, false, new object[] { 10f }, 1, 6, 18));

            enemy.AddOnDeathPattern(enemy.Shoot(LinearRandomWays, Content.Sprite.MediumBullet_R, false, new object[] { 2f, 48 }, 1, 1000, 50));
            enemy.AddMovement(enemy.MoveInAngle(1f, 20f, 1f));

            Manager.Add(enemy);
        }

        public static void Stage1Type18(Vector2 position)
        {
            var enemy = new CommonEnemy(Content.Sprite.Enemy1, position, 60, 2000);

            enemy.AddShootingPattern(enemy.Shoot(LinearStaticRandomWays, Content.Sprite.EllipseBullet_B, false, new object[] { 2f, 24 }, 60, 1000, 0));

            enemy.AddMovement(enemy.MoveInAngleAlt(0f, .005f, 1.04f));
            enemy.AddMovement(enemy.MoveInAngleAlt(90f, .35f, 1f));

            Manager.Add(enemy);
        }

        public static void Stage1Type18_2(Vector2 position)
        {
            var enemy = new CommonEnemy(Content.Sprite.Enemy1, position, 60, 2000);

            enemy.AddShootingPattern(enemy.Shoot(LinearStaticRandomWays, Content.Sprite.EllipseBullet_B, false, new object[] { 2f, 24 }, 65, 650, 0));

            enemy.AddMovement(enemy.MoveInAngleAlt(180f, .005f, 1.04f));
            enemy.AddMovement(enemy.MoveInAngleAlt(90f, .35f, 1f));

            Manager.Add(enemy);
        }

        public static void Stage1Type19(Vector2 position)
        {
            var enemy = new CommonEnemy(Content.Sprite.Enemy1, position, 950, 20000, true);

            enemy.AddShootingPattern(enemy.Shoot(Untitled, Content.Sprite.EllipseBullet_B, false, new object[] { 8f, 6f }, 3, 1000, 1));
            enemy.AddShootingPattern(enemy.Shoot(Untitled, Content.Sprite.EllipseBullet_R, false, new object[] { 4f, 6f }, 3, 1000, 1));

            enemy.AddMovement(enemy.MoveInAngle(90f, 5.7f, 0.98f));

            Manager.Add(enemy);
        }

        public static void Stage1Type19_2(Vector2 position)
        {
            var enemy = new CommonEnemy(Content.Sprite.Enemy1, position, 950, 20000, true, true);

            enemy.AddShootingPattern(enemy.Shoot(Untitled, Content.Sprite.EllipseBullet_B, false, new object[] { 8f, 6f }, 2, 1000, 1));
            enemy.AddShootingPattern(enemy.Shoot(Untitled, Content.Sprite.EllipseBullet_R, false, new object[] { 4f, 6f }, 2, 1000, 1));

            enemy.AddMovement(enemy.MoveInAngle(90f, 5.7f, 0.98f));

            Manager.Add(enemy);
        }

        public override void Kill()
        {
            float hue1 = rand.NextFloat(.2f, 0.5f);
            Color color1 = ColorUtility.HSVToColor(hue1, 0.9f, 1f);
            Color color2 = ColorUtility.HSVToColor(hue1, 0.9f, 4f);

            for (int i = 0; i < 8; i++)
            {
                float speed = 4f * (1f - 1 / rand.NextFloat(0.5f, 1f));
                var state = new Particle.State()
                {
                    Velocity = rand.NextVector2(speed, speed),
                    Type = Particle.ParticleType.EnemyOnDeath,
                    ScaleMultiplier = 6f
                };

                Color color = Color.Lerp(color1, color2, rand.NextFloat(0, 0.1f));
                Main.ParticleManager.CreateParticle(Content.Sprite.CircleFeatheredParticle, Position, color, 128, 1f, state);
            }

            ApplyBehaviors(onDeathPatterns);

            Content.Audio.CommonExplosion.Play(.6f, MathUtility.Rng.NextFloat(-.5f, 0f), 0);
            Status.AddScore(Bounty);

            if (LastEnemy) Status.IsGameCleared = true;

            base.Kill();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            Orientation = Velocity.ToAngle();
        }

        public CommonEnemy(Texture2D image, Vector2 position, int hp, int bounty, bool isImmuneToBomb = false, bool isLast = false)
        {
            this.image = image;
            Position = position;
            Radius = image.Width / 2.5f;
            color = Color.White;
            Bounty = bounty;
            HP = hp;
            BombImmunity = isImmuneToBomb;
            LastEnemy = isLast;
        }
    }
}
