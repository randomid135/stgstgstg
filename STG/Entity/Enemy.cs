﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using System;
using System.Reflection;

namespace STG.Entity
{
    partial class Enemy : SubEntity
    {
        protected static Random rand = new Random();

        public int TargetPriority = -1;

        private const int hitDuration = 5;

        protected static float elapsed = 0;

        private int hitTimer = 0;

        protected int Bounty;

        protected int HP;

        protected bool LastEnemy;

        public bool BombImmunity { get; protected set; }

        protected void AddMovement(IEnumerable<int> movement)
        {
            AddBehavior(movements, movement);
        }

        protected void AddShootingPattern(IEnumerable<int> shootingPattern)
        {
            AddBehavior(shootingPatterns, shootingPattern);
        }

        protected void AddOnDeathPattern(IEnumerable<int> shootingPattern)
        {
            AddBehavior(onDeathPatterns, shootingPattern);
        }

        public void WasShot(int damage)
        {
            HP -= damage;
            hitTimer = hitDuration;

            if (HP < 0) Kill();
        }

        public override void Update(GameTime gameTime)
        {
            ApplyBehaviors(movements);
            ApplyBehaviors(shootingPatterns);

            if (hitTimer <= 0)
            {
                color = Color.White;
            }
            else
            {
                hitTimer--;
                color = Color.Red;
            }

            base.Update(gameTime);

            if (elapsed > 360)
                elapsed = 0;

            elapsed++;
        }

        public void Banish()
        {
            base.Kill();
        }
    }

    partial class Enemy
    {
        protected List<IEnumerator<int>> shootingPatterns = new List<IEnumerator<int>>();
        protected List<IEnumerator<int>> onDeathPatterns = new List<IEnumerator<int>>();

        protected delegate void GenerateBulletAction(Texture2D image, Vector2 position, bool isRect, params object[] arguments);

        protected IEnumerable<int> Shoot(GenerateBulletAction action, Texture2D image, bool isRect, object[] args, int cooldown, int duration, int delay)
        {
            int cooldownTimer = 0;

            int durationTimer = duration;

            while (true)
            {
                if (durationTimer >= 0)
                {
                    if (cooldownTimer <= 0)
                    {
                        cooldownTimer = cooldown;
                        action(image, Position, isRect, args);
                        Content.Audio.EnemyShot.Play(.1f, MathUtility.Rng.NextFloat(-1f, 0.5f), 0);
                    }

                    --cooldownTimer;
                }
                else if (durationTimer <= -delay)
                {
                    durationTimer = duration;
                }

                --durationTimer;

                yield return 0;
            }
        }
    }
}