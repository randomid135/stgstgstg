﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using System;

namespace STG.Entity
{
    partial class EnemyBullet : SubEntity
    {
        public static EnemyBullet LinearBullet(Texture2D image, Vector2 position, bool isRect, float acceleration, float angle)
        {
            var bullet = new EnemyBullet(image, position, isRect);
            bullet.AddBehavior(bullet.movements, bullet.MoveStraight(angle, acceleration));

            return bullet;
        }

        public static EnemyBullet AcceleratingBullet(Texture2D image, Vector2 position, bool isRect, float acceleration, float angle, float multiplier, float min = 0f, float max = 20f)
        {
            var bullet = new EnemyBullet(image, position, isRect);
            bullet.AddBehavior(bullet.movements, bullet.MoveStraight(angle, acceleration, multiplier, min, max));

            return bullet;
        }

        public static EnemyBullet MovingAfterAngleChangesBullet(Texture2D image, Vector2 position, bool isRect, float acceleration, float angle1, float angle2, int duration1, int duration2)
        {
            var bullet = new EnemyBullet(image, position, isRect);
            bullet.AddBehavior(bullet.movements, bullet.MoveAfterAngleChanges(angle1, angle2, duration1, duration2, acceleration));

            return bullet;
        }

        public static EnemyBullet SeekingBullet(Texture2D image, Vector2 position, bool isRect, float acceleration)
        {
            var bullet = new EnemyBullet(image, position, isRect);
            bullet.AddBehavior(bullet.movements, bullet.MoveToPlayer(acceleration));

            return bullet;
        }

        public static EnemyBullet SeekingBulletForceAdded(Texture2D image, Vector2 position, bool isRect, float acceleration, Vector2 cv)
        {
            var bullet = new EnemyBullet(image, position, isRect);
            bullet.AddBehavior(bullet.movements, bullet.MoveToPlayerForceAdded(acceleration, cv));

            return bullet;
        }

        public static EnemyBullet UntitledBullet(Texture2D image, Vector2 position, bool isRect, float acceleration, float factor)
        {
            var bullet = new EnemyBullet(image, position, isRect);
            bullet.AddBehavior(bullet.movements, bullet.Untitled5(acceleration, factor));

            return bullet;
        }

        public static EnemyBullet UntitledBullet_2(Texture2D image, Vector2 position, bool isRect, float acceleration, float factor)
        {
            var bullet = new EnemyBullet(image, position, isRect);
            bullet.AddBehavior(bullet.movements, bullet.Untitled5_2(acceleration, factor));

            return bullet;
        }
    }

    partial class EnemyBullet
    {
        static Random rand = new Random();

        const float disabledTime = 8f;

        int disabledTimeTimer = (int)disabledTime;

        public EnemyBullet(Texture2D image, Vector2 position, bool isRect)
        {
            this.image = image;
            color = Color.Transparent;
            Position = position;
            Radius = image.Height / 4f;
            IsRectCollision = isRect;
        }

        public override void Kill()
        {
            base.Kill();

            for (int i = 0; i < 30; i++)
            {
                float speed = 5f * (1f - 1 / rand.NextFloat(0.7f, 0.85f));
                var state = new Particle.State()
                {
                    Velocity = rand.NextVector2(speed, speed),
                    ScaleMultiplier = 1f,
                };

                Main.ParticleManager.CreateParticle(Content.Sprite.CircleParticle, Position, Color.Black, 200, 1f, state);
            }
        }

        public override void Update(GameTime gameTime)
        {
            if (disabledTimeTimer > 0)
            {
                disabledTimeTimer--;
                color = Color.White * (1 - disabledTimeTimer / disabledTime);
            } 

            ApplyBehaviors(movements);
            base.Update(gameTime);
            Orientation = Velocity.ToAngle();
        }
    }
}
