﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STG.Entity
{
    partial class EnemyBullet
    {

        IEnumerable<int> MoveToPlayer(float acceleration, float offsetX = 0, float offsetY = 0)
        {
            Vector2 offset = new Vector2(offsetX, offsetX);

            Vector2 dp = Player.Instance.Position + offset - Position;

            dp.Normalize();

            while (true)
            {
                Velocity += dp.ScaleTo(acceleration);

                yield return 0;
            }
        }

        IEnumerable<int> MoveToPlayerForceAdded(float acceleration, Vector2 cv, float offsetX = 0, float offsetY = 0)
        {
            Velocity = cv;

            Vector2 offset = new Vector2(offsetX, offsetX);

            
            

            while (true)
            {
                for (int i = 0; i < 120; i++)
                {

                    yield return 0;
                }

                for (int i = 0; i < 10000; i++)
                {
                    Vector2 dp = Player.Instance.Position + offset - Position;

                    var dv = Vector2.Normalize(dp).ScaleTo(acceleration);
                    var sf = dv - Velocity;
                    Velocity += sf;

                    yield return 0;
                }
            }
        }

        IEnumerable<int> MoveToPlayerConstantly(float acceleration)
        {
            while (true)
            {
                Velocity += (Player.Instance.Position - Position).ScaleTo(acceleration);
                if (Velocity != Vector2.Zero)
                    Orientation = Velocity.ToAngle();

                yield return 0;
            }
        }

        IEnumerable<int> MoveStraight(float angle, float acceleration, float multiplier = 1f, float min = 0f, float max = 20f)
        {
            float rad = angle.ToRadian();
            Vector2 velocity = new Vector2((float)Math.Cos(rad), (float)Math.Sin(rad));
            
            while (true)
            {
                Velocity = velocity.ScaleTo(acceleration);
                acceleration = MathHelper.Clamp(acceleration * multiplier, min, max);

                yield return 0;
            }
        }

        IEnumerable<int> MoveAfterAngleChanges(float angle1, float angle2, int duration1, int duration2, float acceleration)
        {
            float rad1 = angle1.ToRadian();
            float rad2 = angle2.ToRadian();

            Vector2 velocity = new Vector2((float)Math.Cos(rad1), (float)Math.Sin(rad1));
            Vector2 velocity2 = new Vector2((float)Math.Cos(rad2), (float)Math.Sin(rad2));

            while (true)
            {

                for (int i = 0; i < duration1; i++)
                {
                    Velocity = velocity.ScaleTo(acceleration);

                    yield return 0;
                }

                for (int i = 0; i < duration2; i++)
                {
                    Velocity = velocity2.ScaleTo(acceleration);

                    yield return 0;
                }

            }
        }

        IEnumerable<int> Untitled(float angle, float acceleration)
        {
            float rad = angle.ToRadian();
            Vector2 velocity1 = new Vector2((float)Math.Cos(rad), (float)Math.Sin(rad));
            while (true)
            {

                for (int i = 0; i < 180; i++)
                {
                    Vector2 velocity = new Vector2((float)Math.Cos(MathUtility.ToRadian(i / 1.2f)), (float)Math.Sin(rad));

                    Velocity = velocity.ScaleTo(2.5f);

                    yield return 0;
                }

                for (int i = 0; i < 360; i++)
                {

                    Velocity = velocity1.ScaleTo(3f);

                    yield return 0;
                }

            }
        }

        IEnumerable<int> Untitled2(float acceleration, float angle)
        {
            float rad = angle.ToRadian();
            float rad1 = (angle + 90).ToRadian();
            Vector2 velocity = new Vector2((float)Math.Cos(rad), (float)Math.Sin(rad));
            Vector2 velocity1 = new Vector2((float)Math.Cos(rad1), (float)Math.Sin(rad1));
            while (true)
            {

                for (int i = 0; i < 30; i++)
                {
                    Velocity = velocity.ScaleTo(2f);

                    yield return 0;
                }

                for (int i = 0; i < 900; i++)
                {

                    Velocity = velocity1.ScaleTo(5f);

                    yield return 0;
                }

            }
        }
        IEnumerable<int> Untitled3(float acceleration, float angle)
        {
            float rad = angle.ToRadian();
            float rad1 = (angle - 90).ToRadian();
            Vector2 velocity = new Vector2((float)Math.Cos(rad), (float)Math.Sin(rad));
            Vector2 velocity1 = new Vector2((float)Math.Cos(rad1), (float)Math.Sin(rad1));
            while (true)
            {

                for (int i = 0; i < 30; i++)
                {
                    Velocity = velocity.ScaleTo(2f);

                    yield return 0;
                }

                for (int i = 0; i < 900; i++)
                {

                    Velocity = velocity1.ScaleTo(5f);

                    yield return 0;
                }

            }
        }

        IEnumerable<int> Untitled4(float acceleration, float angle)
        {
            float rad = angle.ToRadian();
            float rad1 = (angle - 180).ToRadian();
            Vector2 velocity = new Vector2((float)Math.Cos(rad), (float)Math.Sin(rad));
            Vector2 velocity1 = new Vector2((float)Math.Cos(rad1), (float)Math.Sin(rad1));
            while (true)
            {

                for (int i = 0; i < 90; i++)
                {
                    Velocity = velocity.ScaleTo(6f);

                    yield return 0;
                }

                for (int i = 0; i < 900; i++)
                {

                    Velocity = velocity1.ScaleTo(1f);

                    yield return 0;
                }

            }
        }
        IEnumerable<int> Untitled5(float acceleration, float factor)
        {
            while (true)
            {
                float angle = MathUtility.Rng.NextFloat(0, 360);
                for (int i = 0; i < 720; i++)
                {
                    Vector2 velocity = new Vector2((float)Math.Sin((angle + i).ToRadian()), (float)Math.Cos((angle + i).ToRadian()));
                    Velocity = velocity.ScaleTo(factor);

                    yield return 0;
                }

            }
        }

        IEnumerable<int> Untitled5_2(float acceleration, float factor)
        {
            while (true)
            {
                float angle = MathUtility.Rng.NextFloat(0, 360);
                for (int i = 0; i < 720; i++)
                {
                    Vector2 velocity = new Vector2((float)Math.Cos((angle + i).ToRadian()), (float)Math.Sin((angle + i).ToRadian()));
                    Velocity = velocity.ScaleTo(factor);

                    yield return 0;
                }

            }
        }
    }
}
