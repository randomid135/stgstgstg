﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace STG.Entity
{
    //Shooting
    partial class Enemy
    {
        /// <summary>
        /// args = { float acceleration, float angle }
        /// </summary>
        protected static GenerateBulletAction Linear = (Texture2D image, Vector2 position, bool isRect, object[] args) =>
        {
            float acceleration = (float)args[0];
            float angle = (float)args[1];

            Manager.Add(EnemyBullet.LinearBullet(image, position, isRect, acceleration, angle));
        };

        /// <summary>
        /// args = { float acceleration }
        /// </summary>
        protected static GenerateBulletAction LinearRandom = (Texture2D image, Vector2 position, bool isRect, object[] args) =>
        {
            float acceleration = (float)args[0];

            Linear(image, position, isRect, args[0], rand.NextFloat(0, 360));
        };

        /// <summary>
        /// args = { float acceleration, float startAngle, float endAngle }
        /// </summary>
        protected static GenerateBulletAction LinearRandomInRange = (Texture2D image, Vector2 position, bool isRect, object[] args) =>
        {
            float acceleration = (float)args[0];
            float startAngle = (float)args[1];
            float endAngle = (float)args[2];

            Linear(image, position, isRect, args[0], rand.NextFloat(startAngle, endAngle));
        };

        /// <summary>
        /// args = { float acceleration, float initialAngle, int ways }
        /// </summary>
        protected static GenerateBulletAction LinearStaticWays = (Texture2D image, Vector2 position, bool isRect, object[] args) =>
        {
            float acceleration = (float)args[0];
            float initialAngle = (float)args[1];
            int ways = (int)args[2];

            float angle = 360 / ways;

            for (int i = 0; i < ways; i++)
                Linear(image, position, isRect, acceleration, angle * i + initialAngle);

        };

        /// <summary>
        /// args = { float acceleration, int ways }
        /// </summary>
        protected static GenerateBulletAction LinearStaticRandomWays = (Texture2D image, Vector2 position, bool isRect, object[] args) =>
        {
            float acceleration = (float)args[0];
            int ways = (int)args[1];

            float initialAngle = rand.NextFloat(0, 360);

            float angle = 360 / ways;

            for (int i = 0; i < ways; i++)
                Linear(image, position, isRect, acceleration, angle * i + initialAngle);

        };

        protected static GenerateBulletAction LinearRandomWays = (Texture2D image, Vector2 position, bool isRect, object[] args) =>
        {
            float acceleration = (float)args[0];
            int ways = (int)args[1];

            for (int i = 0; i < ways; i++)
                Linear(image, position, isRect, acceleration, rand.NextFloat(0, 360));

        };

        /// <summary>
        /// args = { float acceleration, float initialAngle, int ways, float angleMultiplier }
        /// </summary>
        protected static GenerateBulletAction LinearDynamicWays = (Texture2D image, Vector2 position, bool isRect, object[] args) =>
        {
            float acceleration = (float)args[0];
            float initialAngle = (float)args[1];
            int ways = (int)args[2];
            float angleMultiplier = (float)args[3];

            float angle = 360 / ways;

            for (int i = 0; i < ways; i++)
                Linear(image, position, isRect, acceleration, angle * i + initialAngle + elapsed * angleMultiplier);
        };

        /// <summary>
        /// args = { float acceleration, float angle, float multiplier }
        /// </summary>
        protected static GenerateBulletAction Accelerating = (Texture2D image, Vector2 position, bool isRect, object[] args) =>
        {
            float acceleration = (float)args[0];
            float angle = (float)args[1];
            float multiplier = (float)args[2];

            Manager.Add(EnemyBullet.AcceleratingBullet(image, position, isRect, acceleration, angle, multiplier));
        };

        /// <summary>
        /// args = { float acceleration, float startAngle, float endAngle, float multiplier }
        /// </summary>

        protected static GenerateBulletAction AcceleratingInRange = (Texture2D image, Vector2 position, bool isRect, object[] args) =>
        {
            float acceleration = (float)args[0];
            float startAngle = (float)args[1];
            float endAngle = (float)args[2];
            float multiplier = (float)args[3];

            Accelerating(image, position, isRect, acceleration, MathUtility.Rng.NextFloat(startAngle, endAngle), multiplier);
        };


        /// <summary>
        /// args = { float acceleration, float initialAngle, int ways, float timeMultiplier, float velocityMultiplier }
        /// </summary>
        protected static GenerateBulletAction AcceleratingDynamicWays = (Texture2D image, Vector2 position, bool isRect, object[] args) =>
        {
            float acceleration = (float)args[0];
            float initialAngle = (float)args[1];
            int ways = (int)args[2];
            float timeMultiplier = (float)args[3];
            float velocityMultiplier = (float)args[4];

            float angle = 360 / ways;


            for (int i = 0; i < ways; i++)
                Accelerating(image, position, isRect, acceleration, angle * i + initialAngle + elapsed * timeMultiplier, velocityMultiplier);
        };

        /// <summary>
        /// args = { float acceleration, float angle1, float angle2, int duration1, int duration2 }
        /// </summary>
        protected static GenerateBulletAction MovingAfterAngleChanges = (Texture2D image, Vector2 position, bool isRect, object[] args) =>
        {
            float acceleration = (float)args[0];
            float angle1 = (float)args[1];
            float angle2 = (float)args[2];
            int duration1 = (int)args[3];
            int duration2 = (int)args[4];

            Manager.Add(EnemyBullet.MovingAfterAngleChangesBullet(image, position, isRect, acceleration, angle1, angle2, duration1, duration2));
        };

        /// <summary>
        /// args = { float acceleration, float initialAngle, float angleVariance, int duration1, int duration2 }
        /// </summary>
        protected static GenerateBulletAction MovingAfterAngleChangesRandom = (Texture2D image, Vector2 position, bool isRect, object[] args) =>
        {
            float acceleration = (float)args[0];
            float initialAngle = rand.NextFloat(0, 360);
            float angleVariance = (float)args[1];
            int duration1 = (int)args[2];
            int duration2 = (int)args[3];

            MovingAfterAngleChanges(image, position, isRect, acceleration, initialAngle, initialAngle + angleVariance, duration1, duration2);
        };

        /// <summary>
        /// args = { float acceleration }
        /// </summary>
        protected static GenerateBulletAction Seeking = (Texture2D image, Vector2 position, bool isRect, object[] args) =>
        {
            try
            {
                float acceleration = (float)args[0];
                float initialAngle = (Player.Instance.Position - position).ToAngle().ToDegree();

                Linear(image, position, isRect, acceleration, initialAngle);
            }
            catch (Exception e)
            {
                throw e;
            }
        };

        protected static GenerateBulletAction SeekingForceAdded = (Texture2D image, Vector2 position, bool isRect, object[] args) =>
        {
            try
            {
                float acceleration = (float)args[0];
                float rad = ((float)args[1]).ToRadian();
                Vector2 cv = new Vector2((float)Math.Cos(rad), (float)Math.Sin(rad));
                cv.ScaleTo(acceleration);

                Manager.Add(EnemyBullet.SeekingBulletForceAdded(image, position, isRect, acceleration, cv));
            }
            catch (Exception e)
            {
                throw e;
            }
        };

        /// <summary>
        /// args = { float acceleration, float offsetAngle, int ways }
        /// </summary>
        protected static GenerateBulletAction SeekingStaticWays = (Texture2D image, Vector2 position, bool isRect, object[] args) =>
        {
            float acceleration = (float)args[0];
            float offsetAngle = (float)args[1];
            int ways = (int)args[2];

            float initialAngle = (Player.Instance.Position - position).ToAngle().ToDegree() + offsetAngle;

            float angle = 360 / ways;

            for (int i = 0; i < ways; i++)
                Linear(image, position, isRect, acceleration, angle * i + initialAngle);

        };

        /// <summary>
        /// args = { float acceleration, float variance }
        /// </summary>
        protected static GenerateBulletAction SeekingWithVariance = (Texture2D image, Vector2 position, bool isRect, object[] args) =>
        {
            float acceleration = (float)args[0];
            float variance = (float)args[1];

            float initialAngle = (Player.Instance.Position - position).ToAngle().ToDegree();

            Linear(image, position, isRect, acceleration, initialAngle + rand.NextFloat(-variance / 2, variance / 2));

        };

        /// <summary>
        /// args = { float acceleration, float variance, float multiplier, float min, float max }
        /// </summary>
        protected static GenerateBulletAction AcceleratingSeekingWithVariance = (Texture2D image, Vector2 position, bool isRect, object[] args) =>
        {
            float acceleration = (float)args[0];
            float variance = (float)args[1];
            float multiplier = (float)args[2];
            float min = (float)args[3];
            float max = (float)args[4];

            float initialAngle = (Player.Instance.Position - position).ToAngle().ToDegree();

            Manager.Add(EnemyBullet.AcceleratingBullet(image, position, isRect, acceleration, initialAngle + rand.NextFloat(-variance / 2, variance / 2), multiplier, min, max));
        };

        protected static GenerateBulletAction Untitled = (Texture2D image, Vector2 position, bool isRect, object[] args) =>
        {
            float acceleration = (float)args[0];
            float angle = (float)args[1];

            Manager.Add(EnemyBullet.UntitledBullet(image, position, isRect, acceleration, angle));

        };

        protected static GenerateBulletAction Untitled_2 = (Texture2D image, Vector2 position, bool isRect, object[] args) =>
        {
            float acceleration = (float)args[0];
            float angle = (float)args[1];

            Manager.Add(EnemyBullet.UntitledBullet_2(image, position, isRect, acceleration, angle));

        };
    }

    //Movement
    partial class Enemy
    {
        protected IEnumerable<int> MoveToPlayer(float acceleration)
        {
            Vector2 dp = Player.Instance.Position - Position;
            dp.Normalize();

            while (true)
            {
                Velocity += dp.ScaleTo(acceleration);

                yield return 0;
            }
        }

        protected IEnumerable<int> MoveToPlayerConstantly(float acceleration)
        {
            while (true)
            {
                Velocity += (Player.Instance.Position - Position).ScaleTo(acceleration);
                if (Velocity != Vector2.Zero)
                    Orientation = Velocity.ToAngle();

                yield return 0;
            }
        }

        protected IEnumerable<int> MoveInAngle(float angle, float acceleration, float multiplier)
        {
            float rad = angle.ToRadian();
            Vector2 velocity = new Vector2((float)Math.Cos(rad), (float)Math.Sin(rad));
            while (true)
            {
                Velocity = velocity.ScaleTo(acceleration);
                acceleration *= multiplier;
                yield return 0;
            }
        }

        protected IEnumerable<int> MoveInAngleAlt(float angle, float acceleration, float multiplier)
        {
            float rad = angle.ToRadian();
            Vector2 velocity = new Vector2((float)Math.Cos(rad), (float)Math.Sin(rad));
            while (true)
            {
                Velocity += velocity.ScaleTo(acceleration * .1f);
                acceleration *= multiplier;
                yield return 0;
            }
        }

        protected IEnumerable<int> MoveInASquare()
        {
            const int framesPerSide = 30;
            while (true)
            {

                for (int i = 0; i < framesPerSide; i++)
                {
                    Velocity = Vector2.UnitX * 2;
                    yield return 0;
                }
            }
        }
    }
}
