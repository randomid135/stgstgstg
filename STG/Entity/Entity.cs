﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace STG.Entity
{
    abstract class Entity
    {
        protected Texture2D image;
        protected Color color = Color.White;
        public Vector2 Position, Velocity;
        public float Scale = 1f;
        public float Orientation;
        public float Radius;
        public bool IsRectCollision = false;
        public bool IsExpired;

        public Texture2D Image
        {
            set
            {
                image = value;
            }
        }

        public Vector2 Size
        {
            get
            {
                if (image == null)
                    return Vector2.Zero;
                else
                    return new Vector2(image.Width, image.Height);
            }
        }

        public Vector2 TopLeft
        {
            get
            {
                if (image == null)
                    return Vector2.Zero;
                else
                    return Position - new Vector2(image.Width / 2, image.Height / 2);
            }
        }

        public Vector2 BottomRight
        {
            get
            {
                if (image == null)
                    return Vector2.Zero;
                else
                    return Position + new Vector2(image.Width / 2, image.Height / 2);
            }
        }

        public abstract void Update(GameTime gameTime);

        public virtual void Kill()
        {
            IsExpired = true;
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(image, Position, null, color, Orientation, Size / 2f, Scale, 0, Constant.LayerDepth.Entity);
        }

        public virtual void Draw(SpriteBatch spriteBatch, float layerDepth)
        {
            spriteBatch.Draw(image, Position, null, color, Orientation, Size / 2f, Scale, 0, layerDepth);
        }
    }
}
