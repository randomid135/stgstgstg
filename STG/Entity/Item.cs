﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace STG.Entity
{
    partial class Item : SubEntity
    {
        public static Item Power(Vector2 position)
        {
            var item = new Item(Content.Sprite.LineParticle, position, ItemType.Power);
            item.AddBehavior(item.movements, item.MoveToPlayerConstantly(2.6f));

            return item;
        }

        public static Item Life(Vector2 position)
        {
            var item = new Item(Content.Sprite.CircleParticle, position, ItemType.Life);

            return item;
        }

        public static Item Bomb(Vector2 position)
        {
            var item = new Item(Content.Sprite.EllipseBullet_B, position, ItemType.Bomb);

            return item;
        }
    }

    partial class Item
    {
        private static Random rand = new Random();

        private Color particleColor;

        public enum ItemType
        {
            Power,
            Life,
            Bomb
        }

        public ItemType Type { get; private set; }

        private Item(Texture2D image, Vector2 position, ItemType itemType)
        {
            this.image = image;
            Position = position;
            Radius = image.Height;
            Type = itemType;

            switch (Type)
            {
                case ItemType.Bomb:
                    particleColor = Color.White;
                    break;
                case ItemType.Life:
                    particleColor = Color.OrangeRed;
                    break;
                case ItemType.Power:
                    particleColor = Color.LightSkyBlue;
                    break;
            }

        }

        public override void Update(GameTime gameTime)
        {
            ApplyBehaviors(movements);
            base.Update(gameTime);
        }
    }

    partial class Item
    {
        IEnumerable<int> MoveToPlayerConstantly(float acceleration)
        {
            while (true)
            {
                Velocity += (Player.Instance.Position - Position).ScaleTo(acceleration);
                if (Velocity != Vector2.Zero)
                    Orientation = Velocity.ToAngle();

                GenerateParticle(particleColor);

                yield return 0;
            }
        }
    }

    partial class Item
    {
        private void GenerateParticle(Color color)
        {
            for (int i = 0; i < 30; i++)
            {
                float speed = 2f * (1f - 1 / rand.NextFloat(0.7f, 0.85f));
                var state = new Particle.State()
                {
                    Velocity = rand.NextVector2(speed, speed),
                    ScaleMultiplier = 1f,
                };

                Main.ParticleManager.CreateParticle(Content.Sprite.CircleParticle, Position, color, 60, 0.3f, state);
            }
        }
    }
}
