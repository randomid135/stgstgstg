﻿using System.Collections.Generic;
using System.Linq;
using System;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace STG.Entity
{
    static class Manager
    {
        static List<Entity> entities = new List<Entity>();

        static bool isUpdating;
        static List<Entity> addedEntities = new List<Entity>();

        static List<Enemy> enemies = new List<Enemy>();
        static List<EnemyBullet> enemyBullets = new List<EnemyBullet>();
        static List<Obstacle> obstacles = new List<Obstacle>();
        static List<PlayerBullet> playerBullets = new List<PlayerBullet>();
        static List<Item> items = new List<Item>();
        static List<PlayerLaser> playerLasers = new List<PlayerLaser>();

        public enum EntityListType { Enemies, EnemyBullets, Obstacles, Boss, PlayerBullets, Items };

        private static void AddEntity(Entity entity)
        {
            entities.Add(entity);
            if (entity is PlayerBullet)
                playerBullets.Add(entity as PlayerBullet);
            else if (entity is CommonEnemy)
                enemies.Add(entity as CommonEnemy);
            else if (entity is Obstacle)
                obstacles.Add(entity as Obstacle);
            else if (entity is Boss)
                enemies.Add(entity as Boss);
            else if (entity is EnemyBullet)
                enemyBullets.Add(entity as EnemyBullet);
            else if (entity is Item)
                items.Add(entity as Item);
            else if (entity is PlayerLaser)
                playerLasers.Add(entity as PlayerLaser);
        }

        public static int Count
        {
            get
            {
                return entities.Count();
            }
        }

        public static void Add(Entity entity)
        {
            if (!isUpdating)
                AddEntity(entity);
            else
                addedEntities.Add(entity);
        }

        public static void Clear()
        {
            entities.Clear();
            enemies.Clear();
            enemyBullets.Clear();
            obstacles.Clear();
            playerBullets.Clear();
            items.Clear();
            playerLasers.Clear();
        }

        private static bool IsColliding(Entity a, Entity b)
        {
            float radius = a.Radius + b.Radius;
            
            return !a.IsExpired && !b.IsExpired && Vector2.DistanceSquared(a.Position, b.Position) < radius * radius;
        }
        /*
         * <summary>
         * a is a Circle, b is a Rectangle
         * </summary>
         */

        private static bool IsRectColliding(Entity a, Entity b)
        {
            double uX = Math.Cos(-b.Orientation) * (a.Position.X - b.Position.X) - Math.Sin(-b.Orientation) * (a.Position.Y - b.Position.Y) + b.Position.X;
            double uY = Math.Sin(-b.Orientation) * (a.Position.X - b.Position.X) + Math.Cos(-b.Orientation) * (a.Position.Y - b.Position.Y) + b.Position.Y;

            double cX, cY;

            if (uX < b.TopLeft.X)
                cX = b.TopLeft.X;
            else if (uX > b.TopLeft.X + b.Size.X)
                cX = b.TopLeft.X + b.Size.X;
            else
                cX = uX;

            if (uY < b.TopLeft.Y)
                cY = b.TopLeft.Y;
            else if (uY > b.TopLeft.Y + b.Size.Y)
                cY = b.TopLeft.Y + b.Size.Y;
            else
                cY = uY;

            double distanceSquared = (uX - cX) * (uX - cX) + (uY - cY) * (uY - cY);

            return distanceSquared < a.Radius * a.Radius;
        }

        private static bool IsRectColliding(Entity a, Rectangle b)
        {
            if (b.Width <= 0 || b.Height <= 0)
                return false;
            else
            {
                float cX = MathHelper.Clamp(a.Position.X, b.Left, b.Right);
                float cY = MathHelper.Clamp(a.Position.Y, b.Top, b.Bottom);

                float dX = a.Position.X - cX;
                float dY = a.Position.Y - cY;

                float d = (dX * dX) + (dY * dY);

                return d < (a.Radius * a.Radius);
            }
        }

        private static bool IsOutofBound(Vector2 position)
        {
            return position.X > Constant.Screen.BoundaryEnd.X || position.X < Constant.Screen.BoundaryStart.X || position.Y > Constant.Screen.BoundaryEnd.Y || position.Y < Constant.Screen.BoundaryStart.Y;
        }

        private static bool IsOutofBoundStrict(Vector2 position)
        {
            return position.X > Constant.Screen.StrictBoundaryEnd.X || position.X < Constant.Screen.StrictBoundaryStart.X || position.Y > Constant.Screen.StrictBoundaryEnd.Y || position.Y < Constant.Screen.StrictBoundaryStart.Y;
        }

        public static void ClearBullets()
        {
            enemyBullets.ForEach(b => b.Kill());
        }

        private static void OnPlayerGetsHit()
        {
            Player.Instance.Kill();
            enemyBullets.ForEach(b => b.Kill());
        }

        private static void HandleCollisions()
        {
            // Kill PlayerBullets when they are out of bound
            for (int i = 0; i < playerBullets.Count; i++)
                if (IsOutofBoundStrict(playerBullets[i].Position))
                    playerBullets[i].Kill();

            // Collision Event with Enemies
            for (int i = 0; i < enemies.Count; i++)
            {
                // Kill Enemies when they are hit by PlayerBullets
                for (int j = 0; j < playerBullets.Count; j++)
                    if (IsColliding(enemies[i], playerBullets[j]))
                    {
                        enemies[i].WasShot(playerBullets[j].Damage);
                        playerBullets[j].IsExpired = true;
                    }

                for (int j = 0; j < playerLasers.Count; j++)
                {
                    if (IsRectColliding(enemies[i], playerLasers[j].Rect))
                    {
                        enemies[i].WasShot(playerLasers[j].Damage);
                    }
                }

                // Kill Enemies when they collide with Obstacles
                for (int j = 0; j < obstacles.Count; j++)
                    if (IsRectColliding(enemies[i], obstacles[i]))
                        enemies[i].Kill();

                // Banish Enemies when they are out of bound
                if (IsOutofBound(enemies[i].Position))
                    enemies[i].Banish();

                // Kill Player when the Collision detected
                if (IsColliding(Player.Instance, enemies[i]) && !Player.Instance.IsInvincible && !Status.IsGameCleared && !Status.IsGameOver)
                    OnPlayerGetsHit();
            }

            for (int i = 0; i < enemyBullets.Count; i++)
            {
                if (IsOutofBoundStrict(enemyBullets[i].Position))
                    enemyBullets[i].Kill();

                if (enemyBullets[i].IsRectCollision)
                {
                    if (IsRectColliding(Player.Instance, enemyBullets[i]) && !Player.Instance.IsInvincible && !Status.IsGameCleared && !Status.IsGameOver)
                        OnPlayerGetsHit();
                }
                else
                {
                    if (IsColliding(Player.Instance, enemyBullets[i]) && !Player.Instance.IsInvincible && !Status.IsGameCleared && !Status.IsGameOver)
                    OnPlayerGetsHit();
                }

            }

            for (int i = 0; i < obstacles.Count; i++)
            {
                if (IsRectColliding(Player.Instance, obstacles[i]))
                    OnPlayerGetsHit();
            }

            for (int i = 0; i < items.Count; i++)
            {
                if (IsOutofBound(items[i].Position))
                    items[i].Kill();

                if (IsColliding(items[i], Player.Instance))
                {
                    var type = items[i].Type;

                    switch (type)
                    {
                        case Item.ItemType.Power:
                            Status.AddPower();
                            break;
                        case Item.ItemType.Life:
                            Status.AddLife();
                            break;
                        case Item.ItemType.Bomb:
                            Status.AddBomb();
                            break;
                    }

                    items[i].Kill();
                }
            }
        }

        public static void Update(GameTime gameTime)
        {
            try
            {
                isUpdating = true;

                HandleCollisions();

                foreach (var entity in entities)
                    entity.Update(gameTime);

                isUpdating = false;

                foreach (var entity in addedEntities)
                    AddEntity(entity);

                addedEntities.Clear();

                entities = entities.Where(x => !x.IsExpired).ToList();
                enemies = enemies.Where(x => !x.IsExpired).ToList();
                obstacles = obstacles.Where(x => !x.IsExpired).ToList();
                playerBullets = playerBullets.Where(x => !x.IsExpired).ToList();
                enemyBullets = enemyBullets.Where(x => !x.IsExpired).ToList();
                items = items.Where(x => !x.IsExpired).ToList();
            }
            catch (Exception e)
            {
                
            }
            
        }

        public static IEnumerable<Entity> GetEntitiesInRange(Vector2 position, float radius)
        {
            return entities.Where(e => Vector2.DistanceSquared(position, e.Position) < radius * radius);
        }

        public static IEnumerable<Enemy> GetExistingEnemies()
        {
            return enemies.Where(e => e is Enemy);
        }

        public static void Draw(SpriteBatch spriteBatch)
        {
            try
            {
                if (!isUpdating)
                    foreach (var entity in entities)
                    {

                        entity.Draw(spriteBatch);
                    }

            }
            catch (Exception e)
            {

            }
        }

        public static void DrawEnemies(SpriteBatch spriteBatch)
        {
            try
            {
                if (!isUpdating)
                    foreach (var entity in enemies)
                    {

                        entity.Draw(spriteBatch);
                    }

            }
            catch (Exception e)
            {

            }
        }

        public static void DrawEnemyBullets(SpriteBatch spriteBatch)
        {
            try
            {
                if (!isUpdating)
                    foreach (var entity in enemyBullets)
                    {

                        entity.Draw(spriteBatch);
                    }

            }
            catch (Exception e)
            {

            }
        }

        public static void DrawObstacles(SpriteBatch spriteBatch)
        {
            try
            {
                if (!isUpdating)
                    foreach (var entity in obstacles)
                    {

                        entity.Draw(spriteBatch);
                    }

            }
            catch (Exception e)
            {

            }
        }

        public static void DrawPlayerBullets(SpriteBatch spriteBatch)
        {
            try
            {
                if (!isUpdating)
                    foreach (var entity in playerBullets)
                    {

                        entity.Draw(spriteBatch);
                    }

            }
            catch (Exception e)
            {

            }
        }

        public static void DrawItems(SpriteBatch spriteBatch)
        {
            try
            {
                if (!isUpdating)
                    foreach (var entity in items)
                    {

                        entity.Draw(spriteBatch);
                    }

            }
            catch (Exception e)
            {

            }
        }

        public static void DrawPlayerLasers(SpriteBatch spriteBatch)
        {
            try
            {
                if (!isUpdating)
                    foreach (var entity in playerLasers)
                    {
                        entity.Draw(spriteBatch);
                    }

            }
            catch (Exception e)
            {

            }
        }
    }
}
