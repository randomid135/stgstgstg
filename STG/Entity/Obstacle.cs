﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STG.Entity
{
    class Obstacle : SubEntity
    {
        public static void Obs1 (Vector2 position)
        {
            var obs = new Obstacle(Content.Sprite.LongBullet, position);
            //obs.AddMovement(obs.Rotate(1f));
            obs.AddMovement(obs.MoveInAngle(57f, 1f, 1.03f));

            Manager.Add(obs);
        }

        public Obstacle(Texture2D image, Vector2 position)
        {
            this.image = image;
            Position = position;
            color = Color.White;
            IsRectCollision = true;
        }

        IEnumerable<int> Rotate(float angle)
        {
            float rad = angle.ToRadian();
            while (true)
            {
                Orientation += rad;
                yield return 0;
            }
        }

        IEnumerable<int> MoveInAngle(float angle, float acceleration, float multiplier)
        {
            float rad = angle.ToRadian();
            Vector2 velocity = new Vector2((float)Math.Cos(rad), (float)Math.Sin(rad));
            while (true)
            {
                Velocity += velocity.ScaleTo(acceleration);
                acceleration *= multiplier;
                yield return 0;
            }
        }

        private void AddMovement(IEnumerable<int> movement)
        {
            AddBehavior(movements, movement);
        }

        public override void Update(GameTime gameTime)
        {
            ApplyBehaviors(movements);
            base.Update(gameTime);
            Orientation = Velocity.ToAngle();
        }
    }
}
