﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Linq;

namespace STG.Entity
{
    class Player : Entity
    {
        Vector2 startPoint = new Vector2(Main.ScreenSize.X / 2, Main.ScreenSize.Y - 60);

        int fadeTimer = 0;

        int invincibilityTimer = 0;

        public static int SlowSpeed => 3;
        public static int NormalSpeed => 9;        

        public bool IsDead { get { return fadeTimer > 0; } }

        public bool IsInvincible { get { return invincibilityTimer > 0; } }

        PlayerLaser playerLaser;

        private static Player instance;
        public static Player Instance
        {
            get
            {
                if (instance == null)
                    instance = new Player();
                return instance;
            }
        }

        Player()
        {
            image = Content.Sprite.Player;
            Position = startPoint;
            Radius = 4;
            playerLaser = new PlayerLaser(Content.Sprite.Laser, 1);
            Manager.Add(playerLaser);
        }

        public void Bomb()
        {
            invincibilityTimer = 180;
            Status.RemoveBomb();

            var closestEnemies = Manager.GetExistingEnemies().OrderBy(e => Vector2.DistanceSquared(e.Position, Position));

            int count = closestEnemies.Count();
            int maxCount = count < 9 ? count : 8;

            int priority = 0;

            foreach (var enemy in closestEnemies)
            {
                if (priority > 7) break;
                if (enemy.BombImmunity) continue;

                enemy.TargetPriority = priority;
                ++priority;
            }

            for (int i = 0; i < 8; ++i)
            {
                Content.Audio.PlayerBomb.Play(.5f, MathUtility.Rng.NextFloat(-0.7f, 0.7f), 0);
                Manager.Add(PlayerBullet.SeekingPlayerBullet(Content.Sprite.EllipseBullet_G, Position, i % priority, 180 + i * 4, 1f, 50));
            }
        }

        public void FireMainShot()
        {
            playerLaser.Inflate(4, 0);
        }

        public void FireSubShot(float angleVariance)
        {
            var lPos = Vector2.Add(Position, new Vector2(-10, 6));
            var lPos2 = Vector2.Add(Position, new Vector2(-20, 6));
            var rPos = Vector2.Add(Position, new Vector2(10, 6));
            var rPos2 = Vector2.Add(Position, new Vector2(20, 6));

            Manager.Add(PlayerBullet.NormalPlayerBullet(Content.Sprite.PlayerBullet3, lPos, 272f + angleVariance / 2, 25f, 1));
            Manager.Add(PlayerBullet.NormalPlayerBullet(Content.Sprite.PlayerBullet3, rPos, 268f - angleVariance / 2, 25f, 1));
            Manager.Add(PlayerBullet.NormalPlayerBullet(Content.Sprite.PlayerBullet2, lPos2, 275f + angleVariance, 25f, 1));
            Manager.Add(PlayerBullet.NormalPlayerBullet(Content.Sprite.PlayerBullet2, rPos2, 265f - angleVariance, 25f, 1));
        }

        public override void Kill()
        {
            Status.RemoveLife();
            Status.ResetBomb();
            Status.ResetMultiplier();

            fadeTimer = 120;

            float hue1 = MathUtility.Rng.NextFloat(.1f, .5f);
            float hue2 = (hue1 + MathUtility.Rng.NextFloat(0, .5f));
            Color color1 = ColorUtility.HSVToColor(hue1, 0.9f, 1);
            Color color2 = ColorUtility.HSVToColor(hue2, 0.7f, 1);

            for (int i = 0; i < 16; i++)
            {
                float speed = 10f * (1f - 1 / MathUtility.Rng.NextFloat(0.4f, 1.1f));
                var state = new Particle.State()
                {
                    Velocity = MathUtility.Rng.NextVector2(speed, speed),
                    ScaleMultiplier = 10f,
                    Type = Particle.ParticleType.EnemyOnDeath
                };

                Color color = Color.Lerp(color2, color1, MathUtility.Rng.NextFloat(0, 1));
                Main.ParticleManager.CreateParticle(Content.Sprite.CircleParticle, Position, color, 120, MathUtility.Rng.NextFloat(0.4f, 2f), state);
            }

            Content.Audio.PlayerExplosion.Play(.4f, MathUtility.Rng.NextFloat(-0.2f, 0.2f), 0);

            invincibilityTimer = 180;
        }
        
        public override void Update(GameTime gameTime)
        {
            playerLaser.Inflate(-2, 0);

            if (!IsDead && !Status.IsGameOver)
            {
                Input.PlayerInput.Update(gameTime);
                invincibilityTimer--;
                Position = Vector2.Clamp(Position, Constant.Screen.ScreenBorderStart + Size, Constant.Screen.ScreenBorderEnd - Size);
            }
            else
            {
                if (Status.IsGameOver)
                {
                    Position = new Vector2(-1000, -1000);
                }
                else
                {
                    Position = new Vector2(Main.ScreenSize.X / 2, Main.ScreenSize.Y - 60 + fadeTimer);
                    fadeTimer--;
                    return;
                }
            }

            if (invincibilityTimer == 20)
            {
                for (int i = 0; i < 16; i++)
                {
                    float speed = 14f * (1f - 1 / MathUtility.Rng.NextFloat(0.5f, 1f));
                    var state = new Particle.State()
                    {
                        Velocity = MathUtility.Rng.NextVector2(speed, speed),
                        ScaleMultiplier = 1f,
                        Type = Particle.ParticleType.EnemyOnDeath
                    };

                    Main.ParticleManager.CreateParticle(Content.Sprite.CircleParticle, Position, Color.White, 100, MathUtility.Rng.NextFloat(0.4f, 1f), state);
                }

                var entities = Manager.GetEntitiesInRange(Position, 250f);

                foreach (var entity in entities)
                {
                    if (entity is EnemyBullet eb)
                    {
                        eb.Kill();
                        Status.AddMultiplier();
                    }
                }
            }
        }

        public void Reset()
        {
            instance = null;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (!Status.IsGameOver)
            {
                playerLaser.Draw(spriteBatch);

                if (!IsInvincible)
                    base.Draw(spriteBatch, 1f);
                    
                else
                    spriteBatch.Draw(Content.Sprite.Player_Bomb, Position, null, Color.White, Orientation, new Vector2(40, 40), 1f, 0, 1f);
            }
                
        }
    }
}
