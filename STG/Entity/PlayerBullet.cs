﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;

namespace STG.Entity
{
    class PlayerBullet : SubEntity
    {
        int damage;

        const float disabledTime = 4f;
        float disabledTimeTimer = disabledTime;
        bool isTrailActive = false;

        public int Damage
        {
            get
            {
                return damage;
            }
        }

        public static PlayerBullet NormalPlayerBullet(Texture2D image, Vector2 position, float angle, float acceleration, int damage)
        {
            var bullet = new PlayerBullet(image, position, damage);
            bullet.AddBehavior(bullet.movements, bullet.MoveStraight(angle, acceleration));

            return bullet;
        }

        public static PlayerBullet SeekingPlayerBullet(Texture2D image, Vector2 position, int priority, float initAngle, float acceleration, int damage)
        {
            var bullet = new PlayerBullet(image, position, damage);
            bullet.AddBehavior(bullet.movements, bullet.MoveToEnemy(priority, initAngle, acceleration));
            bullet.isTrailActive = true;

            return bullet;
        }

        public PlayerBullet(Texture2D image, Vector2 position, int damage)
        {
            this.image = image;
            Position = position;
            this.damage = damage;
            color = Color.Transparent;
        }

        public override void Update(GameTime gameTime)
        {
            if (disabledTimeTimer > 0)
            {
                disabledTimeTimer--;
                color = Color.White * (1 - disabledTimeTimer / disabledTime) * 0.7f;
            }

            if (isTrailActive)
            {
                for (int j = 0; j < 1; j++)
                {
                    var state = new Particle.State()
                    {
                        Velocity = Velocity,
                        Type = Particle.ParticleType.FadeOnly,
                        ScaleMultiplier = 1f
                    };

                    Main.ParticleManager.CreateParticle(Content.Sprite.CircleParticle, Position, Color.DarkKhaki, 32, .75f, state);
                }
            }

            base.Update(gameTime);
            ApplyBehaviors(movements);
            if (Velocity != Vector2.Zero)
                Orientation = Velocity.ToAngle();
        }

        protected void AddMovement(IEnumerable<int> movement)
        {
            AddBehavior(movements, movement);
        }

        IEnumerable<int> MoveStraight(float angle, float acceleration)
        {
            float rad = angle.ToRadian();
            Vector2 velocity = new Vector2((float)Math.Cos(rad), (float)Math.Sin(rad));

            Velocity = velocity.ScaleTo(acceleration);
            while (true)
            {
                yield return 0;
            }
        }

        IEnumerable<int> MoveToEnemy(int priority, float initAngle, float acceleration)
        {

            float rad = initAngle.ToRadian();
            Vector2 velocity = new Vector2((float)Math.Cos(rad), (float)Math.Sin(rad));

            Velocity = velocity.ScaleTo(acceleration);

            Vector2 nullPos = new Vector2(-987, -789);

            while (true)
            {
                for (int i = 0; i < 1000; ++i)
                {
                    Vector2 targetPos = Manager.GetExistingEnemies().SingleOrDefault(e => e.TargetPriority.Equals(priority))?.Position ?? nullPos;

                    if (Vector2.Equals(targetPos, nullPos))
                    {
                        Kill();
                    }

                    Vector2 dp = targetPos - Position;

                    var dv = Vector2.Normalize(dp).ScaleTo(acceleration);
                    var sf = dv - Velocity;

                    Velocity += sf;
                    acceleration *= 1.035f;

                    yield return 0;
                }
            }
        }
    }
}
