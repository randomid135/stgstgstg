﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STG.Entity
{
    class PlayerLaser : SubEntity
    {
        Rectangle rect;

        const int defaultThreshold = 24;

        int threshold = defaultThreshold;

        float colorInterpolation = 0f;
        int colorMuliplier = 1;

        public int Damage { get; private set; }

        public bool IsEnabled = true;

        static readonly Color defaultColorBegin = ColorUtility.HSVToColor(3.5f, 1f, 1);
        static readonly Color defaultColorEnd = ColorUtility.HSVToColor(4f, 1f, 1);

        Color color1 = defaultColorBegin;
        Color color2 = defaultColorEnd;

        public Rectangle Rect
        {
            get
            {
                return rect;
            }
        }

        public PlayerLaser(Texture2D image, int damage)
        {
            this.image = image;
            Damage = damage;
            rect = new Rectangle(Vector2.Zero.ToPoint(), new Point(0, 1500));
        }

        public void Inflate(int w, int h)
        {
            rect.Inflate(w, h);
        }

        public override void Update(GameTime gameTime)
        {
            rect.Width = (int)MathHelper.Clamp(rect.Width, 0, threshold);
            rect.X = (int)Player.Instance.Position.X - rect.Width / 2;
            rect.Y = (int)Player.Instance.Position.Y - rect.Height;

            colorInterpolation += (float)gameTime.ElapsedGameTime.TotalSeconds * colorMuliplier;

            if (colorInterpolation > 1f || colorInterpolation < 0f)
                colorMuliplier *= -1;
        }

        private Color GetLaserColor()
        {
            return Color.Lerp(color2, color1, colorInterpolation);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (!Player.Instance.IsDead)
            {
                spriteBatch.Draw(image, rect, null, new Color(255, 255, 255, 128), 0f, Vector2.Zero, SpriteEffects.None, 0.8f);
                spriteBatch.Draw(Content.Sprite.CircleParticle, rect, GetLaserColor() * 0.5f);
            }
                
        }
    }
}
