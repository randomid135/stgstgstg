﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace STG.GameState
{
    class Ending : GameState
    {
        public Ending(GraphicsDevice graphics, SpriteBatch spriteBatch) : base(graphics, spriteBatch) { }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            spriteBatch.DrawStringCentered(Content.Sprite.MainFont, "Thank you for Playing", new Vector2(640, 200), Color.White);
            spriteBatch.DrawStringCentered(Content.Sprite.MainFont, "You've made it!", new Vector2(640, 280), Color.White, .6f);
            spriteBatch.DrawStringCentered(Content.Sprite.MainFont, "Your Score: " + Status.Score, new Vector2(640, 480), Color.White, .75f);
            spriteBatch.DrawStringCentered(Content.Sprite.MainFont, "2018081024", new Vector2(100, 920), Color.White, .5f);
            spriteBatch.DrawStringCentered(Content.Sprite.MainFont, "Press ESC or Z key to go back to the title menu", new Vector2(640, 800), Color.White, .5f);
            spriteBatch.End();
        }

        public override void Initialize()
        {
        }

        public override void Update(GameTime gameTime)
        {
            HandleInput();
        }

        protected override void HandleInput()
        {
            if (Input.Manager.OnKeyDown(Keys.Z) || Input.Manager.OnKeyDown(Keys.Escape))
                Manager.RemoveScene();
        }
    }
}
