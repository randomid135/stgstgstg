﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STG.GameState
{
    static class Manager
    {
        static Stack<GameState> scenes = new Stack<GameState>();

        public static Boolean IsPaused { get; private set; } = false;

        public static void Pause()
        {
            IsPaused = true;
        }

        static void Unpause()
        {
            IsPaused = false;
        }

        public static void AddScene(GameState scene)
        {
            try
            {
                scenes.Push(scene);
                scenes.Peek().Initialize();
            }
            catch (Exception e)
            {

            }
        }

        public static void RemoveScene()
        {
            if (scenes.Count > 0)
            {
                try
                {
                    scenes.Pop();
                    Unpause();
                }
                catch (Exception e)
                {

                }
            }
            
        }

        public static void Update(GameTime gameTime)
        {
            if (scenes.Count > 0)
            {
                try
                {
                    scenes.Peek().Update(gameTime);

                }
                catch (Exception e)
                {

                }
            }
        }

        public static void Initialize()
        {
            if (scenes.Count > 0)
            {
                try
                {
                    scenes.Peek().Initialize();

                }
                catch (Exception e)
                {

                }
            }

        }

        public static void Draw(SpriteBatch spriteBatch)
        {
            if (scenes.Count > 0)
            {
                try
                {
                    scenes.Peek().Draw(spriteBatch);

                }
                catch (Exception e)
                {

                }
            }
        }
    }
}
