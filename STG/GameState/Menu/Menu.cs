﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STG.GameState.Menu
{
    class Menu : GameState
    {
        protected String[] menus;
        protected Vector2 initPos;

        protected int itr;
        protected int Itr
        {
            get
            {
                return itr;
            }

            set
            {
                if (value > menus.Length - 1)
                    itr = 0;
                else if (value < 0)
                    itr = menus.Length - 1;
                else
                    itr = value;
            }
        }

        public Menu(GraphicsDevice graphics, SpriteBatch spriteBatch, String[] menus, Vector2 initPos) : base(graphics, spriteBatch)
        {
            this.menus = menus;
            this.initPos = initPos;
            Itr = 0;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            graphics.Clear(Color.Black);

            spriteBatch.Begin();

            for (int i = 0; i < menus.Length; ++i)
            {
                var color = Color.Gray;

                if (Itr == i)
                    color = Color.White;

                spriteBatch.DrawString(Content.Sprite.MainFont, menus[i], initPos + new Vector2(0, i * 64), color, 0f, Vector2.Zero, 0.5f, SpriteEffects.None, 1f);
            }

            spriteBatch.End();
        }

        public override void Initialize()
        {

        }

        public override void Update(GameTime gameTime)
        {
            HandleInput();
        }

        protected override void HandleInput()
        {
            if (Input.Manager.OnKeyDown(Keys.Up))
                --Itr;
            else if (Input.Manager.OnKeyDown(Keys.Down))
                ++Itr;
            else if (Input.Manager.OnKeyDown(Keys.Z))
                Select();
        }

        protected virtual void Select()
        {
            
        }
    }
}
