﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace STG.GameState.Menu
{
    class Settings : Menu
    {
        int itr2;
        int Itr2
        {
            get
            {
                return itr2;
            }

            set
            {
                if (value >= 0 && value <= 100)
                    itr2 = value;
            }
        }

        int[] currentSettings = { 0 };

        public Settings(GraphicsDevice graphics, SpriteBatch spriteBatch, string[] menus, Vector2 initPos) : base(graphics, spriteBatch, menus, initPos) { }

        protected override void HandleInput()
        {
            base.HandleInput();
            if (Input.Manager.OnKeyDown(Keys.Left))
                --Itr2;
            else if (Input.Manager.OnKeyDown(Keys.Right))
                ++Itr2;
        }

        protected override void Select()
        {
            switch (Itr)
            {
                case 0:
                    Main.Instance.ToggleFullScreen();
                    if (Main.Instance.IsFullScreen)
                        menus[0] = "FullScreen: ON";
                    else
                        menus[0] = "FullScreen: OFF";
                    break;
                case 1:
                    Manager.RemoveScene();
                    break;
            }
        }
    }
}
