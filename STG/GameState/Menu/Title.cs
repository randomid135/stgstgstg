﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace STG.GameState.Menu
{
    class Title : Menu
    {
        public Title(GraphicsDevice graphics, SpriteBatch spriteBatch, string[] menus, Vector2 initPos) : base(graphics, spriteBatch, menus, initPos) {}

        protected override void Select()
        {
            switch (Itr)
            {
                case 0:
                    Manager.AddScene(new OnPlay(graphics, spriteBatch));
                    break;
                case 1:
                    Manager.AddScene(new Settings(graphics, spriteBatch, new string[] { "FullScreen: Off", "Back" }, initPos));
                    break;
                case 2:
                    Main.Instance.Exit();
                    break;
            }
        }
    }
}
