﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace STG.GameState
{
    public class OnPlay : GameState
    {
        UI.Sidebar sidebar = new UI.Sidebar();
        UI.Curtain curtain = new UI.Curtain();

        Stage.Spawner spawner = new Stage.Spawner(1);
        Content.Background bg = Content.BG.background;
        int alpha = 0;

        public OnPlay(GraphicsDevice graphicsDevice, SpriteBatch spriteBatch) : base(graphicsDevice, spriteBatch) {}

        public override void Draw(SpriteBatch spriteBatch)
        {
            // Start PostProcessing

            Main.Instance.bloom.Begin();

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);
                Entity.Manager.DrawPlayerBullets(spriteBatch);
                Entity.Player.Instance.Draw(spriteBatch);
                Entity.Manager.DrawEnemyBullets(spriteBatch);
                Entity.Manager.DrawEnemies(spriteBatch);
            spriteBatch.End();

            Main.Instance.bloom.End();

            Main.Instance.bloom2.Begin();

                spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.Additive);
                    Main.ParticleManager.Draw(spriteBatch);
                spriteBatch.End();

            Main.Instance.bloom2.End();

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.Opaque, SamplerState.LinearWrap);
                bg.Draw(spriteBatch);
            spriteBatch.End();

            spriteBatch.Begin();
                spriteBatch.Draw(Main.Instance.bloom2.FinalRenderTarget, new Rectangle(0, 0, Main.screenW, Main.screenH), Color.White);
            spriteBatch.End();

            spriteBatch.Begin();
                spriteBatch.Draw(Main.Instance.bloom.FinalRenderTarget, new Rectangle(0, 0, Main.screenW, Main.screenH), Color.White);
            spriteBatch.End();

            spriteBatch.Begin(SpriteSortMode.Deferred);
                sidebar.Draw(spriteBatch);
            spriteBatch.End();

            spriteBatch.Begin(SpriteSortMode.Deferred);
                curtain.Draw(spriteBatch, alpha / 2);
            spriteBatch.End();
        }

        public override void Initialize()
        {
            graphics.Clear(Color.Black);
            Status.Reset();
            Entity.Player.Instance.Reset();
            Entity.Manager.Clear();
            Entity.Manager.Add(Entity.Player.Instance);
        }

        public override void Update(GameTime gameTime)
        {
            spawner.Update(gameTime);
            Entity.Manager.Update(gameTime);
            bg.Update();
            HandleInput();

            if (Status.IsGameOver)
            {
                alpha += 2;

                if (alpha > 600)
                {
                    Manager.RemoveScene();
                }
                    
            }

            if (Status.IsGameCleared && !Status.IsGameOver)
            {
                alpha += 1;

                if (alpha > 600)
                {
                    Manager.RemoveScene();
                    Manager.AddScene(new Ending(graphics, spriteBatch));
                }
                    
            }
        }

        protected override void HandleInput()
        {
            if (Input.Manager.OnKeyDown(Keys.Escape))
                Manager.AddScene(new Paused(graphics, spriteBatch));
        }
    }
}
