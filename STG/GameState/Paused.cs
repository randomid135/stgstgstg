﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace STG.GameState
{
    class Paused : GameState
    {
        public Paused(GraphicsDevice graphics, SpriteBatch spriteBatch) : base(graphics, spriteBatch) {}

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            spriteBatch.DrawStringCentered(Content.Sprite.MainFont, "Paused", new Vector2(640, 480), Color.White);
            spriteBatch.DrawStringCentered(Content.Sprite.MainFont, "Press ESC or Z key to resume", new Vector2(640, 780), Color.White, .5f);
            spriteBatch.DrawStringCentered(Content.Sprite.MainFont, "Press X key to return to the title menu", new Vector2(640, 820), Color.White, .5f);
            spriteBatch.End();
        }

        public override void Initialize()
        {
            Manager.Pause();
        }

        public override void Update(GameTime gameTime)
        {
            HandleInput();
        }

        protected override void HandleInput()
        {
            if (Input.Manager.OnKeyDown(Keys.Z) || Input.Manager.OnKeyDown(Keys.Escape))
                Manager.RemoveScene();
            else if (Input.Manager.OnKeyDown(Keys.X))
            {
                Manager.RemoveScene();
                Manager.RemoveScene();
            }
                
        }
    }
}
