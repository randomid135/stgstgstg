﻿using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STG.Input
{
    public static class Manager
    {
        static KeyboardState oldKeyboardState;
        static KeyboardState newKeyboardState;

        public static void Update()
        {
            oldKeyboardState = newKeyboardState;
            newKeyboardState = Keyboard.GetState();
        }

        public static bool IsKeyDown(Keys key)
        {
            return newKeyboardState.IsKeyDown(key);
        }

        public static bool IsKeyUp(Keys key)
        {
            return newKeyboardState.IsKeyUp(key);
        }

        public static bool OnKeyDown(Keys key)
        {
            return newKeyboardState.IsKeyDown(key) && oldKeyboardState.IsKeyUp(key);
        }
    }
}
