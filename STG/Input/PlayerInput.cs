﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace STG.Input
{
    class PlayerInput
    {
        // To Player class
        static int playerSpeed = 8;

        static int subShotCooldown = Entity.Player.NormalSpeed;

        static float angleVariance = 10f;

        static float AngleVariance
        {
            get
            {
                return angleVariance;
            }
            set
            {
                if (value > -1 && value < 21)
                    angleVariance = value;
            }
        }

        public static void Update(GameTime gameTime)
        {
            if (Manager.IsKeyDown(Keys.Left))
                Entity.Player.Instance.Position.X -= playerSpeed;

            if (Manager.IsKeyDown(Keys.Right))
                Entity.Player.Instance.Position.X += playerSpeed;

            if (Manager.IsKeyDown(Keys.Up))
                Entity.Player.Instance.Position.Y -= playerSpeed;

            if (Manager.IsKeyDown(Keys.Down))
                Entity.Player.Instance.Position.Y += playerSpeed;

            if (Manager.IsKeyDown(Keys.LeftShift))
            {
                playerSpeed = Entity.Player.SlowSpeed;
                --AngleVariance;
            }
            else
            {
                playerSpeed = Entity.Player.NormalSpeed;
                ++AngleVariance;
            }

            if (Manager.OnKeyDown(Keys.Z))
                Content.Audio.PlayerShot.Play(1f, MathUtility.Rng.NextFloat(-0.2f, 0.2f), 0);

            if (Manager.IsKeyDown(Keys.Z))
            {
                Entity.Player.Instance.FireMainShot();

                if (subShotCooldown < 0)
                {
                    Entity.Player.Instance.FireSubShot(AngleVariance);
                    Content.Audio.PlayerShot.Play(.2f, MathUtility.Rng.NextFloat(-0.8f, 0.8f), 0);
                    subShotCooldown = 10;
                }
                
            }

            if (Manager.OnKeyDown(Keys.X))
            {
                if (Status.Bombs > 0 && !Entity.Player.Instance.IsInvincible)
                {
                    Entity.Player.Instance.Bomb();
                }
            }

            --subShotCooldown;
        }
    }
}
