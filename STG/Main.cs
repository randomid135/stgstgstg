﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.IO;

namespace STG
{

    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Main : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        PresentationParameters pp;
        public static int screenW, screenH;

        public Content.CustomEffect.Bloom bloom;
        public Content.CustomEffect.Bloom bloom2;

        public bool IsFullScreen
        {
            get
            {
                return graphics.IsFullScreen;
            }
        }

        public static Main Instance { get; private set; }
        public static Viewport Viewport
        {
            get
            {
                return Instance.GraphicsDevice.Viewport;
            }
        }
        public static Vector2 ScreenSize
        {
            get
            {
                return new Vector2(Viewport.Width, Viewport.Height);
            }
        }

        public static Particle.Manager<Particle.State> ParticleManager { get; private set; }

        public Main()
        {
            Instance = this;

            //Resolution and Fullscreen
            graphics = new GraphicsDeviceManager(this)
            {
                PreferredBackBufferWidth = 1280,
                PreferredBackBufferHeight = 960,
                IsFullScreen = false
            };

            graphics.GraphicsProfile = GraphicsProfile.HiDef;

            Content.RootDirectory = "Content";
            Window.Position = new Point((GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width / 2) - (graphics.PreferredBackBufferWidth / 2), (GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height / 2) - (graphics.PreferredBackBufferHeight / 2));

        }

        public void ToggleFullScreen()
        {
            if (graphics.IsFullScreen)
                DisableFullScreen();
            else
                EnableFullScreen();

            graphics.ApplyChanges();
        }

        public void EnableFullScreen()
        {
            graphics.IsFullScreen = true;
        }

        public void DisableFullScreen()
        {
            graphics.IsFullScreen = false;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            spriteBatch = new SpriteBatch(GraphicsDevice);
            pp = GraphicsDevice.PresentationParameters;
            screenW = pp.BackBufferWidth;
            screenH = pp.BackBufferHeight;
            Rectangle screenRect = new Rectangle(0, 0, screenW, screenH);
            bloom = new STG.Content.CustomEffect.Bloom(graphics.GraphicsDevice, spriteBatch);
            bloom2 = new STG.Content.CustomEffect.Bloom(graphics.GraphicsDevice, spriteBatch);
            bloom2.Settings = STG.Content.CustomEffect.BloomSettings.PresetSettings[2];

            if (!Score.Manager.Exists)
                Score.Manager.WriteScore("Default", 100000);

            ParticleManager = new Particle.Manager<Particle.State>(1024 * 16, Particle.State.UpdateParticle);

            GameState.Manager.Initialize();

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            
            STG.Content.Sprite.LoadContent(Content);
            STG.Content.Audio.LoadContent(Content);
            STG.Content.EffectSource.LoadContent(Content);
            bloom.LoadContent(pp);
            bloom2.LoadContent(pp);

            GameState.Manager.AddScene(new GameState.Menu.Title(graphics.GraphicsDevice, spriteBatch, new string[] { "Play", "Settings", "Quit" }, new Vector2(500, 100)));
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // TODO: Add your update logic here
            ParticleManager.Update();
            Input.Manager.Update();

            GameState.Manager.Update(gameTime);

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            graphics.GraphicsDevice.Clear(Color.TransparentBlack);
            GameState.Manager.Draw(spriteBatch);

            base.Draw(gameTime);
        }
    }
}
