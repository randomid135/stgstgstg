﻿using Microsoft.Xna.Framework;
using System;
using System.Security.Cryptography;

namespace STG
{
    static class MathUtility
    {
        public static readonly Random Rng = new Random();

        public static float ToAngle(this Vector2 vector)
        {
            return (float)Math.Atan2(vector.Y, vector.X);
        }

        public static Vector2 ScaleTo(this Vector2 vector, float length)
        {
            return vector * (length / vector.Length());
        }

        public static Vector2 Truncate(this Vector2 vector, float maxLength)
        {
            if (vector.Length() > maxLength)
            {
                vector.Normalize();
                return vector * maxLength;
            }

            return vector;
        }

        public static float ToDegree(this float angle)
        {
            return angle * (180f / (float)Math.PI);
        }

        public static float ToRadian(this float angle)
        {
            return (float)Math.PI * (angle / 180f);
        }

        public static float NextFloat(this Random rand, float minValue, float maxValue)
        {
            return (float)rand.NextDouble() * (maxValue - minValue) + minValue;
        }

        public static Vector2 NextVector2(this Random rand, float minLength, float maxLength)
        {
            double theta = rand.NextDouble() * 2 * Math.PI;
            float length = rand.NextFloat(minLength, maxLength);

            return new Vector2(length * (float)Math.Cos(theta), length * (float)Math.Sin(theta));
        }

        public static int RandomInt(int min, int max)
        {
            using (RNGCryptoServiceProvider rg = new RNGCryptoServiceProvider())
            {
                byte[] rno = new byte[5];
                rg.GetBytes(rno);
                int randomvalue = BitConverter.ToInt32(rno, 0);
                return randomvalue;
            }
        }
    }
}
