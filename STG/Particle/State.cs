﻿using Microsoft.Xna.Framework;
using System;

namespace STG.Particle
{
    public enum ParticleType { EnemyOnHit, EnemyOnDeath, FadeOnly }

    public struct State
    {
        public Vector2 Velocity;
        public ParticleType Type;
        public float ScaleMultiplier;
        public float Gravity;

        public static void UpdateParticle(Manager<State>.Particle particle)
        {
            var velocity = particle.State.Velocity;
            
            particle.Position += velocity;
            particle.Orientation = velocity.ToAngle();

            float speed = velocity.Length();
            float alpha = Math.Min(1, Math.Min(particle.Life * 2, speed * 1f));
            alpha *= alpha;

            particle.Color.A = (byte)(255 * alpha);

            switch(particle.State.Type)
            {
                case ParticleType.EnemyOnHit:
                    particle.Scale *= particle.State.ScaleMultiplier * Math.Min(Math.Min(1f, 0.2f * speed + 0.1f), alpha);
                    break;
                case ParticleType.FadeOnly:
                    break;
                default:
                    particle.Scale.Y = particle.Scale.X = particle.State.ScaleMultiplier * Math.Min(Math.Min(1f, 0.2f * speed + 0.1f), alpha);
                    break;
            }
            

            if (Math.Abs(velocity.X) + Math.Abs(velocity.Y) < 0.0000001f)
                velocity = Vector2.Zero;

            velocity += particle.State.Gravity * new Vector2(0, 1);
            velocity *= 0.9f;
            particle.State.Velocity = velocity;
        }
    }
}
