﻿using Microsoft.Xna.Framework;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace STG.Stage
{
    class Spawner
    {
        Schema[] stage;
        int delayTimer;
        int itr = 0;

        public Spawner(int level)
        {
            switch (level)
            {
                case 1:
                    stage = Parse(StageDocument.one);
                    break;
            }
        }

        void InvokeMethod<T>(string method, object[] args)
        {
            MethodInfo methodInfo = typeof(T).GetMethod(method, BindingFlags.Public | BindingFlags.Static);
            methodInfo.Invoke(null, args);
        }

        Schema[] Parse(string document)
        {
            var result = JsonConvert.DeserializeObject<IEnumerable<Schema>>(document).ToArray();
            return result;
        }

        void Spawn(Schema stageData)
        {
            object[] args = { stageData.Position };

            if (delayTimer > stageData.Delay)
            {
                switch (stageData.Type)
                {
                    case Schema.EntityType.Enemy:
                        InvokeMethod<Entity.CommonEnemy>(stageData.Name, args);
                        break;
                    case Schema.EntityType.Boss:
                        InvokeMethod<Entity.Boss>(stageData.Name, args);
                        break;
                    case Schema.EntityType.Obstacle:
                        InvokeMethod<Entity.Obstacle>(stageData.Name, args);
                        break;
                    case Schema.EntityType.EndOfStage:
                        Status.IsBossApproaching = true;
                        break;
                }
            }
        }

        public void Update(GameTime gameTime)
        {
            delayTimer += gameTime.ElapsedGameTime.Milliseconds;

            if (itr < stage.Count())
            {
                if (delayTimer > stage[itr].Delay)
                {
                    Spawn(stage[itr]);
                    ++itr;
                    delayTimer = 0;
                }
            }
            else
                return;
        }
    }
}
