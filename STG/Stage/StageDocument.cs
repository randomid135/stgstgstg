﻿namespace STG.Stage
{
    /*
     * Range of X is 160 ~ 1020
     * Range of Y is 0 ~ 960
     * 
     * Types: CommonEnemy, Boss, Obstacle
     */

    public static class StageDocument
    {
        public static string one = @"
[
{
type: 'Enemy',
position: [220, 200],
name: 'Stage1Type1',
delay: 5000
},
{
type: 'Enemy',
position: [1060, 200],
name: 'Stage1Type1_2',
delay: 0
},
{
type: 'Enemy',
position: [220, 200],
name: 'Stage1Type1',
delay: 1500
},
{
type: 'Enemy',
position: [1060, 200],
name: 'Stage1Type1_2',
delay: 0
},
{
type: 'Enemy',
position: [220, 200],
name: 'Stage1Type1',
delay: 5000
},
{
type: 'Enemy',
position: [1060, 200],
name: 'Stage1Type1_2',
delay: 0
},
{
type: 'Enemy',
position: [220, 200],
name: 'Stage1Type1',
delay: 1500
},
{
type: 'Enemy',
position: [1060, 200],
name: 'Stage1Type1_2',
delay: 0
},
{
type: 'Enemy',
position: [220, 200],
name: 'Stage1Type1',
delay: 5000
},
{
type: 'Enemy',
position: [1060, 200],
name: 'Stage1Type1_2',
delay: 0
},
{
type: 'Enemy',
position: [220, 200],
name: 'Stage1Type1',
delay: 1500
},
{
type: 'Enemy',
position: [1060, 200],
name: 'Stage1Type1_2',
delay: 0
},
{
type: 'Enemy',
position: [880, 1080],
name: 'Stage1Type2',
delay: 0
},
{
type: 'Enemy',
position: [400, 1080],
name: 'Stage1Type2',
delay: 0
},
{
type: 'Enemy',
position: [480, -100],
name: 'Stage1Type3',
delay: 4000
},
{
type: 'Enemy',
position: [640, -100],
name: 'Stage1Type3',
delay: 3500
},
{
type: 'Enemy',
position: [800, -100],
name: 'Stage1Type3',
delay: 3000
},
{
type: 'Enemy',
position: [640, 0],
name: 'Stage1Type3_2',
delay: 8000
},
{
type: 'Enemy',
position: [480, -60],
name: 'Stage1Type3_2',
delay: 3000
},
{
type: 'Enemy',
position: [800, -60],
name: 'Stage1Type3_2',
delay: 0
},
{
type: 'Enemy',
position: [320, -120],
name: 'Stage1Type3_2',
delay: 2000
},
{
type: 'Enemy',
position: [960, -120],
name: 'Stage1Type3_2',
delay: 0
},
{
type: 'Enemy',
position: [220, 0],
name: 'Stage1Type2',
delay: 12000
},
{
type: 'Enemy',
position: [220, 0],
name: 'Stage1Type2',
delay: 1000
},
{
type: 'Enemy',
position: [220, 0],
name: 'Stage1Type2',
delay: 1000
},
{
type: 'Enemy',
position: [220, 0],
name: 'Stage1Type2',
delay: 1000
},
{
type: 'Enemy',
position: [220, 0],
name: 'Stage1Type2',
delay: 1000
},
{
type: 'Enemy',
position: [220, 0],
name: 'Stage1Type2',
delay: 1000
},
{
type: 'Enemy',
position: [220, 0],
name: 'Stage1Type2',
delay: 1000
},
{
type: 'Enemy',
position: [220, 0],
name: 'Stage1Type2',
delay: 1000
},
{
type: 'Enemy',
position: [1080, 0],
name: 'Stage1Type2_2',
delay: 2000
},
{
type: 'Enemy',
position: [1080, 0],
name: 'Stage1Type2_2',
delay: 1000
},
{
type: 'Enemy',
position: [1080, 0],
name: 'Stage1Type2_2',
delay: 1000
},
{
type: 'Enemy',
position: [1080, 0],
name: 'Stage1Type2_2',
delay: 1000
},
{
type: 'Enemy',
position: [1080, 0],
name: 'Stage1Type2_2',
delay: 1000
},
{
type: 'Enemy',
position: [1080, 0],
name: 'Stage1Type2_2',
delay: 1000
},
{
type: 'Enemy',
position: [1080, 0],
name: 'Stage1Type2_2',
delay: 1000
},
{
type: 'Enemy',
position: [1080, 0],
name: 'Stage1Type2_2',
delay: 1000
},
{
type: 'Enemy',
position: [500, 0],
name: 'Stage1Type4',
delay: 3000
},
{
type: 'Enemy',
position: [780, 0],
name: 'Stage1Type4',
delay: 0
},
{
type: 'Enemy',
position: [640, -100],
name: 'Stage1Type4_2',
delay: 3000
},
{
type: 'Enemy',
position: [840, -50],
name: 'Stage1Type4_3',
delay: 3000
},
{
type: 'Enemy',
position: [440, -50],
name: 'Stage1Type4_3',
delay: 0
},
{
type: 'Enemy',
position: [220, -100],
name: 'Stage1Type5',
delay: 6000
},
{
type: 'Enemy',
position: [320, -120],
name: 'Stage1Type5',
delay: 500
},
{
type: 'Enemy',
position: [1060, -100],
name: 'Stage1Type6',
delay: 1000
},
{
type: 'Enemy',
position: [960, -120],
name: 'Stage1Type6',
delay: 500
},
{
type: 'Enemy',
position: [640, -100],
name: 'Stage1Type7',
delay: 2000
},
{
type: 'Enemy',
position: [220, -100],
name: 'Stage1Type5',
delay: 1000
},
{
type: 'Enemy',
position: [320, -120],
name: 'Stage1Type5',
delay: 500
},
{
type: 'Enemy',
position: [1060, -100],
name: 'Stage1Type6',
delay: 1000
},
{
type: 'Enemy',
position: [960, -120],
name: 'Stage1Type6',
delay: 500
},
{
type: 'Enemy',
position: [480, -100],
name: 'Stage1Type7',
delay: 0
},
{
type: 'Enemy',
position: [220, -100],
name: 'Stage1Type5',
delay: 1000
},
{
type: 'Enemy',
position: [320, -120],
name: 'Stage1Type5',
delay: 500
},
{
type: 'Enemy',
position: [1060, -100],
name: 'Stage1Type6',
delay: 1000
},
{
type: 'Enemy',
position: [960, -120],
name: 'Stage1Type6',
delay: 500
},
{
type: 'Enemy',
position: [800, -100],
name: 'Stage1Type7',
delay: 0
},
{
type: 'Enemy',
position: [220, -100],
name: 'Stage1Type5',
delay: 1000
},
{
type: 'Enemy',
position: [320, -120],
name: 'Stage1Type5',
delay: 500
},
{
type: 'Enemy',
position: [1060, -100],
name: 'Stage1Type6',
delay: 1000
},
{
type: 'Enemy',
position: [960, -120],
name: 'Stage1Type6',
delay: 500
},
{
type: 'Enemy',
position: [720, -100],
name: 'Stage1Type8',
delay: 3000
},
{
type: 'Enemy',
position: [220, -100],
name: 'Stage1Type5',
delay: 1000
},
{
type: 'Enemy',
position: [320, -120],
name: 'Stage1Type5',
delay: 500
},
{
type: 'Enemy',
position: [1060, -100],
name: 'Stage1Type6',
delay: 1000
},
{
type: 'Enemy',
position: [960, -120],
name: 'Stage1Type6',
delay: 500
},
{
type: 'Enemy',
position: [220, -100],
name: 'Stage1Type5',
delay: 1000
},
{
type: 'Enemy',
position: [320, -120],
name: 'Stage1Type5',
delay: 500
},
{
type: 'Enemy',
position: [1060, -100],
name: 'Stage1Type6',
delay: 1000
},
{
type: 'Enemy',
position: [960, -120],
name: 'Stage1Type6',
delay: 500
},
{
type: 'Enemy',
position: [220, -100],
name: 'Stage1Type5',
delay: 1000
},
{
type: 'Enemy',
position: [320, -120],
name: 'Stage1Type5',
delay: 500
},
{
type: 'Enemy',
position: [1060, -100],
name: 'Stage1Type6',
delay: 1000
},
{
type: 'Enemy',
position: [960, -120],
name: 'Stage1Type6',
delay: 500
},
{
type: 'Enemy',
position: [520, -100],
name: 'Stage1Type8',
delay: 500
},
{
type: 'Enemy',
position: [220, -100],
name: 'Stage1Type5',
delay: 1000
},
{
type: 'Enemy',
position: [320, -120],
name: 'Stage1Type5',
delay: 500
},
{
type: 'Enemy',
position: [1060, -100],
name: 'Stage1Type6',
delay: 1000
},
{
type: 'Enemy',
position: [960, -120],
name: 'Stage1Type6',
delay: 500
},
{
type: 'Enemy',
position: [220, -100],
name: 'Stage1Type5',
delay: 1000
},
{
type: 'Enemy',
position: [320, -120],
name: 'Stage1Type5',
delay: 500
},
{
type: 'Enemy',
position: [1060, -100],
name: 'Stage1Type6',
delay: 1000
},
{
type: 'Enemy',
position: [960, -120],
name: 'Stage1Type6',
delay: 500
},
{
type: 'Enemy',
position: [220, -100],
name: 'Stage1Type5',
delay: 1000
},
{
type: 'Enemy',
position: [320, -120],
name: 'Stage1Type5',
delay: 500
},
{
type: 'Enemy',
position: [1060, -100],
name: 'Stage1Type6',
delay: 1000
},
{
type: 'Enemy',
position: [960, -120],
name: 'Stage1Type6',
delay: 500
},
{
type: 'Enemy',
position: [320, 0],
name: 'Stage1Type9',
delay: 3000
},
{
type: 'Enemy',
position: [960, 0],
name: 'Stage1Type9_2',
delay: 2000
},
{
type: 'Enemy',
position: [320, 0],
name: 'Stage1Type9',
delay: 1500
},
{
type: 'Enemy',
position: [960, 0],
name: 'Stage1Type9_2',
delay: 1500
},
{
type: 'Enemy',
position: [320, 0],
name: 'Stage1Type9',
delay: 1000
},
{
type: 'Enemy',
position: [960, 0],
name: 'Stage1Type9_2',
delay: 1000
},
{
type: 'Enemy',
position: [320, 0],
name: 'Stage1Type9',
delay: 800
},
{
type: 'Enemy',
position: [960, 0],
name: 'Stage1Type9_2',
delay: 800
},
{
type: 'Enemy',
position: [320, 0],
name: 'Stage1Type9',
delay: 800
},
{
type: 'Enemy',
position: [960, 0],
name: 'Stage1Type9_2',
delay: 800
},
{
type: 'Enemy',
position: [320, 0],
name: 'Stage1Type9',
delay: 500
},
{
type: 'Enemy',
position: [960, 0],
name: 'Stage1Type9_2',
delay: 500
},
{
type: 'Enemy',
position: [320, 0],
name: 'Stage1Type9',
delay: 500
},
{
type: 'Enemy',
position: [960, 0],
name: 'Stage1Type9_2',
delay: 500
},
{
type: 'Enemy',
position: [320, 0],
name: 'Stage1Type9',
delay: 500
},
{
type: 'Enemy',
position: [960, 0],
name: 'Stage1Type9_2',
delay: 500
},
{
type: 'Enemy',
position: [320, 0],
name: 'Stage1Type9',
delay: 500
},
{       
type: 'Enemy',
position: [960, 0],
name: 'Stage1Type9_2',
delay: 500
},
{
type: 'Enemy',
position: [320, 0],
name: 'Stage1Type10',
delay: 3000
},
{       
type: 'Enemy',
position: [960, 0],
name: 'Stage1Type10_2',
delay: 200
},
{
type: 'Enemy',
position: [320, 0],
name: 'Stage1Type10',
delay: 200
},
{       
type: 'Enemy',
position: [960, 0],
name: 'Stage1Type10_2',
delay: 200
},
{
type: 'Enemy',
position: [320, 0],
name: 'Stage1Type10',
delay: 200
},
{       
type: 'Enemy',
position: [960, 0],
name: 'Stage1Type10_2',
delay: 200
},
{
type: 'Enemy',
position: [320, 0],
name: 'Stage1Type10',
delay: 200
},
{       
type: 'Enemy',
position: [960, 0],
name: 'Stage1Type10_2',
delay: 200
},
{
type: 'Enemy',
position: [320, 0],
name: 'Stage1Type10',
delay: 200
},
{       
type: 'Enemy',
position: [960, 0],
name: 'Stage1Type10_2',
delay: 200
},
{
type: 'Enemy',
position: [320, 0],
name: 'Stage1Type10',
delay: 200
},
{       
type: 'Enemy',
position: [960, 0],
name: 'Stage1Type10_2',
delay: 200
},
{
type: 'Enemy',
position: [320, 0],
name: 'Stage1Type10',
delay: 200
},
{       
type: 'Enemy',
position: [960, 0],
name: 'Stage1Type10_2',
delay: 200
},
{
type: 'Enemy',
position: [320, 0],
name: 'Stage1Type10',
delay: 200
},
{       
type: 'Enemy',
position: [960, 0],
name: 'Stage1Type10_2',
delay: 200
},
{
type: 'Enemy',
position: [320, 0],
name: 'Stage1Type10',
delay: 200
},
{       
type: 'Enemy',
position: [960, 0],
name: 'Stage1Type10_2',
delay: 200
},
{
type: 'Enemy',
position: [320, 0],
name: 'Stage1Type10',
delay: 200
},
{       
type: 'Enemy',
position: [960, 0],
name: 'Stage1Type10_2',
delay: 200
},
{
type: 'Enemy',
position: [320, 0],
name: 'Stage1Type10',
delay: 200
},
{       
type: 'Enemy',
position: [960, 0],
name: 'Stage1Type10_2',
delay: 200
},
{
type: 'Enemy',
position: [320, 0],
name: 'Stage1Type10',
delay: 200
},
{       
type: 'Enemy',
position: [960, 0],
name: 'Stage1Type10_2',
delay: 200
},
{
type: 'Enemy',
position: [320, 0],
name: 'Stage1Type10',
delay: 200
},
{       
type: 'Enemy',
position: [960, 0],
name: 'Stage1Type10_2',
delay: 200
},
{
type: 'Enemy',
position: [320, 0],
name: 'Stage1Type10',
delay: 200
},
{       
type: 'Enemy',
position: [960, 0],
name: 'Stage1Type10_2',
delay: 200
},
{
type: 'Enemy',
position: [320, 0],
name: 'Stage1Type10',
delay: 200
},
{       
type: 'Enemy',
position: [960, 0],
name: 'Stage1Type10_2',
delay: 200
},
{
type: 'Enemy',
position: [320, 0],
name: 'Stage1Type10',
delay: 200
},
{       
type: 'Enemy',
position: [960, 0],
name: 'Stage1Type10_2',
delay: 200
},
{
type: 'Enemy',
position: [320, 0],
name: 'Stage1Type10',
delay: 200
},
{       
type: 'Enemy',
position: [960, 0],
name: 'Stage1Type10_2',
delay: 200
},
{       
type: 'Enemy',
position: [960, 0],
name: 'Stage1Type10_2',
delay: 200
},
{
type: 'Enemy',
position: [320, 0],
name: 'Stage1Type10',
delay: 200
},
{       
type: 'Enemy',
position: [960, 0],
name: 'Stage1Type10_2',
delay: 200
},
{
type: 'Enemy',
position: [320, 0],
name: 'Stage1Type10',
delay: 200
},
{       
type: 'Enemy',
position: [960, 0],
name: 'Stage1Type10_2',
delay: 200
},
{
type: 'Enemy',
position: [320, 0],
name: 'Stage1Type10',
delay: 200
},
{       
type: 'Enemy',
position: [960, 0],
name: 'Stage1Type10_2',
delay: 200
},
{
type: 'Enemy',
position: [320, 0],
name: 'Stage1Type10',
delay: 200
},
{       
type: 'Enemy',
position: [960, 0],
name: 'Stage1Type10_2',
delay: 200
},
{
type: 'Enemy',
position: [320, 0],
name: 'Stage1Type10',
delay: 200
},
{       
type: 'Enemy',
position: [960, 0],
name: 'Stage1Type10_2',
delay: 200
},
{
type: 'Enemy',
position: [320, 0],
name: 'Stage1Type10',
delay: 200
},
{       
type: 'Enemy',
position: [960, 0],
name: 'Stage1Type10_2',
delay: 200
},
{
type: 'Enemy',
position: [320, 0],
name: 'Stage1Type10',
delay: 200
},
{       
type: 'Enemy',
position: [960, 0],
name: 'Stage1Type10_2',
delay: 200
},
{
type: 'Enemy',
position: [320, 0],
name: 'Stage1Type10',
delay: 200
},
{       
type: 'Enemy',
position: [960, 0],
name: 'Stage1Type10_2',
delay: 200
},
{
type: 'Enemy',
position: [320, 0],
name: 'Stage1Type10',
delay: 200
},
{       
type: 'Enemy',
position: [960, 0],
name: 'Stage1Type10_2',
delay: 200
},
{
type: 'Enemy',
position: [320, 0],
name: 'Stage1Type10',
delay: 200
},
{       
type: 'Enemy',
position: [960, 0],
name: 'Stage1Type10_2',
delay: 200
},
{
type: 'Enemy',
position: [640, 0],
name: 'Stage1Type11',
delay: 1500
},
{
type: 'Enemy',
position: [980, 1060],
name: 'Stage1Type11_2',
delay: 1500
},
{
type: 'Enemy',
position: [300, 1060],
name: 'Stage1Type11_2',
delay: 1500
},
{
type: 'Enemy',
position: [980, 1060],
name: 'Stage1Type11_2',
delay: 1500
},
{
type: 'Enemy',
position: [300, 1060],
name: 'Stage1Type11_2',
delay: 1500
},
{
type: 'Enemy',
position: [640, 0],
name: 'Stage1Type11',
delay: 1500
},
{
type: 'Enemy',
position: [300, 0],
name: 'Stage1Type12',
delay: 4500
},
{
type: 'Enemy',
position: [980, 0],
name: 'Stage1Type12_2',
delay: 3000
},
{
type: 'Enemy',
position: [300, 0],
name: 'Stage1Type12',
delay: 6000
},
{
type: 'Enemy',
position: [980, 0],
name: 'Stage1Type12_2',
delay: 6000
},
{
type: 'Enemy',
position: [780, 0],
name: 'Stage1Type13',
delay: 14000
},
{
type: 'Enemy',
position: [678, 0],
name: 'Stage1Type13_2',
delay: 1000
},
{
type: 'Enemy',
position: [567, -50],
name: 'Stage1Type13',
delay: 900
},
{
type: 'Enemy',
position: [830, 0],
name: 'Stage1Type13',
delay: 800
},
{
type: 'Enemy',
position: [390, -100],
name: 'Stage1Type13_3',
delay: 500
},
{
type: 'Enemy',
position: [620, -10],
name: 'Stage1Type13_2',
delay: 500
},
{
type: 'Enemy',
position: [500, 0],
name: 'Stage1Type13_3',
delay: 500
},
{
type: 'Enemy',
position: [860, -40],
name: 'Stage1Type13_3',
delay: 500
},
{
type: 'Enemy',
position: [520, -20],
name: 'Stage1Type13',
delay: 500
},
{
type: 'Enemy',
position: [630, -100],
name: 'Stage1Type13',
delay: 300
},
{
type: 'Enemy',
position: [370, -20],
name: 'Stage1Type13_2',
delay: 300
},
{
type: 'Enemy',
position: [890, -100],
name: 'Stage1Type13_3',
delay: 300
},
{
type: 'Enemy',
position: [470, -10],
name: 'Stage1Type13_3',
delay: 300
},
{
type: 'Enemy',
position: [380, -30],
name: 'Stage1Type13_2',
delay: 300
},
{
type: 'Enemy',
position: [740, -100],
name: 'Stage1Type13_2',
delay: 300
},
{
type: 'Enemy',
position: [480, 0],
name: 'Stage1Type13_2',
delay: 500
},
{
type: 'Enemy',
position: [780, -20],
name: 'Stage1Type13',
delay: 500
},
{
type: 'Enemy',
position: [850, -20],
name: 'Stage1Type13_3',
delay: 500
},
{
type: 'Enemy',
position: [500, 0],
name: 'Stage1Type13_3',
delay: 500
},
{
type: 'Enemy',
position: [360, 0],
name: 'Stage1Type13_2',
delay: 500
},
{
type: 'Enemy',
position: [640, -40],
name: 'Stage1Type13_2',
delay: 500
},
{
type: 'Enemy',
position: [820, -100],
name: 'Stage1Type13_2',
delay: 300
},
{
type: 'Enemy',
position: [480, 0],
name: 'Stage1Type13_2',
delay: 500
},
{
type: 'Enemy',
position: [780, -20],
name: 'Stage1Type13',
delay: 500
},
{
type: 'Enemy',
position: [520, -20],
name: 'Stage1Type13_3',
delay: 500
},
{
type: 'Enemy',
position: [860, -40],
name: 'Stage1Type13_3',
delay: 500
},
{
type: 'Enemy',
position: [520, -20],
name: 'Stage1Type13',
delay: 500
},
{
type: 'Enemy',
position: [630, -100],
name: 'Stage1Type13',
delay: 500
},
{
type: 'Enemy',
position: [365, -40],
name: 'Stage1Type13_3',
delay: 500
},
{
type: 'Enemy',
position: [520, -20],
name: 'Stage1Type13',
delay: 500
},
{
type: 'Enemy',
position: [577, -100],
name: 'Stage1Type13',
delay: 500
},
{
type: 'Enemy',
position: [390, -100],
name: 'Stage1Type13_3',
delay: 500
},
{
type: 'Enemy',
position: [482, -10],
name: 'Stage1Type13_2',
delay: 500
},
{
type: 'Enemy',
position: [500, 0],
name: 'Stage1Type13_3',
delay: 500
},
{
type: 'Enemy',
position: [480, 0],
name: 'Stage1Type13_2',
delay: 500
},
{
type: 'Enemy',
position: [780, -20],
name: 'Stage1Type13',
delay: 500
},
{
type: 'Enemy',
position: [520, -20],
name: 'Stage1Type13_3',
delay: 500
},
{
type: 'Enemy',
position: [860, -40],
name: 'Stage1Type13_3',
delay: 500
},
{
type: 'Enemy',
position: [520, -20],
name: 'Stage1Type13',
delay: 500
},
{
type: 'Enemy',
position: [630, -100],
name: 'Stage1Type13',
delay: 500
},
{
type: 'Enemy',
position: [365, -40],
name: 'Stage1Type13_3',
delay: 500
},
{
type: 'Enemy',
position: [520, -20],
name: 'Stage1Type13',
delay: 500
},
{
type: 'Enemy',
position: [577, -100],
name: 'Stage1Type13',
delay: 500
},
{
type: 'Enemy',
position: [390, -100],
name: 'Stage1Type13_3',
delay: 500
},
{
type: 'Enemy',
position: [500, 0],
name: 'Stage1Type13_3',
delay: 500
},
{
type: 'Enemy',
position: [360, 0],
name: 'Stage1Type13_2',
delay: 500
},
{
type: 'Enemy',
position: [640, -40],
name: 'Stage1Type13_2',
delay: 500
},
{
type: 'Enemy',
position: [820, -100],
name: 'Stage1Type13_2',
delay: 300
},
{
type: 'Enemy',
position: [480, 0],
name: 'Stage1Type13_2',
delay: 500
},
{
type: 'Enemy',
position: [780, -20],
name: 'Stage1Type13',
delay: 500
},
{
type: 'Enemy',
position: [520, -20],
name: 'Stage1Type13_3',
delay: 500
},
{
type: 'Enemy',
position: [860, -40],
name: 'Stage1Type13_3',
delay: 500
},
{
type: 'Enemy',
position: [520, -20],
name: 'Stage1Type13',
delay: 500
},
{
type: 'Enemy',
position: [630, -100],
name: 'Stage1Type13',
delay: 500
},
{
type: 'Enemy',
position: [365, -40],
name: 'Stage1Type13_3',
delay: 500
},
{
type: 'Enemy',
position: [520, -20],
name: 'Stage1Type13',
delay: 500
},
{
type: 'Enemy',
position: [577, -100],
name: 'Stage1Type13',
delay: 500
},
{
type: 'Enemy',
position: [390, -100],
name: 'Stage1Type13_3',
delay: 500
},
{
type: 'Enemy',
position: [482, -10],
name: 'Stage1Type13_2',
delay: 500
},
{
type: 'Enemy',
position: [500, 0],
name: 'Stage1Type13_3',
delay: 500
},
{
type: 'Enemy',
position: [480, 0],
name: 'Stage1Type13_2',
delay: 500
},
{
type: 'Enemy',
position: [780, -20],
name: 'Stage1Type13',
delay: 500
},
{
type: 'Enemy',
position: [520, -20],
name: 'Stage1Type13_3',
delay: 500
},
{
type: 'Enemy',
position: [780, -20],
name: 'Stage1Type13',
delay: 500
},
{
type: 'Enemy',
position: [520, -20],
name: 'Stage1Type13_3',
delay: 500
},
{
type: 'Enemy',
position: [860, -40],
name: 'Stage1Type13_3',
delay: 500
},
{
type: 'Enemy',
position: [520, -20],
name: 'Stage1Type13',
delay: 500
},
{
type: 'Enemy',
position: [630, -100],
name: 'Stage1Type13',
delay: 500
},
{
type: 'Enemy',
position: [365, -40],
name: 'Stage1Type13_3',
delay: 500
},
{
type: 'Enemy',
position: [520, -20],
name: 'Stage1Type13',
delay: 500
},
{
type: 'Enemy',
position: [577, -100],
name: 'Stage1Type13',
delay: 500
},
{
type: 'Enemy',
position: [390, -100],
name: 'Stage1Type13_3',
delay: 500
},
{
type: 'Enemy',
position: [500, 0],
name: 'Stage1Type13_3',
delay: 500
},
{
type: 'Enemy',
position: [360, 0],
name: 'Stage1Type13_2',
delay: 500
},
{
type: 'Enemy',
position: [640, -40],
name: 'Stage1Type13_2',
delay: 500
},
{
type: 'Enemy',
position: [820, -100],
name: 'Stage1Type13_2',
delay: 300
},
{
type: 'Enemy',
position: [480, 0],
name: 'Stage1Type13_2',
delay: 500
},
{
type: 'Enemy',
position: [390, -100],
name: 'Stage1Type13_3',
delay: 500
},
{
type: 'Enemy',
position: [500, 0],
name: 'Stage1Type13_3',
delay: 500
},
{
type: 'Enemy',
position: [360, 0],
name: 'Stage1Type13_2',
delay: 500
},
{
type: 'Enemy',
position: [520, -20],
name: 'Stage1Type13',
delay: 500
},
{
type: 'Enemy',
position: [577, -100],
name: 'Stage1Type13',
delay: 500
},
{
type: 'Enemy',
position: [390, -100],
name: 'Stage1Type13_3',
delay: 500
},
{
type: 'Enemy',
position: [482, -10],
name: 'Stage1Type13_2',
delay: 500
},
{
type: 'Enemy',
position: [500, 0],
name: 'Stage1Type13_3',
delay: 500
},
{
type: 'Enemy',
position: [480, 0],
name: 'Stage1Type13_2',
delay: 500
},
{
type: 'Enemy',
position: [780, -20],
name: 'Stage1Type13',
delay: 500
},
{
type: 'Enemy',
position: [520, -20],
name: 'Stage1Type13_3',
delay: 500
},
{
type: 'Enemy',
position: [780, -20],
name: 'Stage1Type13',
delay: 500
},
{
type: 'Enemy',
position: [520, -20],
name: 'Stage1Type13_3',
delay: 500
},
{
type: 'Enemy',
position: [860, -40],
name: 'Stage1Type13_3',
delay: 500
},
{
type: 'Enemy',
position: [520, -20],
name: 'Stage1Type13',
delay: 500
},
{
type: 'Enemy',
position: [630, -100],
name: 'Stage1Type13',
delay: 500
},
{
type: 'Enemy',
position: [365, -40],
name: 'Stage1Type13_3',
delay: 500
},
{
type: 'Enemy',
position: [520, -20],
name: 'Stage1Type13',
delay: 500
},
{
type: 'Enemy',
position: [577, -100],
name: 'Stage1Type13',
delay: 500
},
{
type: 'Enemy',
position: [390, -100],
name: 'Stage1Type13_3',
delay: 500
},
{
type: 'Enemy',
position: [500, 0],
name: 'Stage1Type13_3',
delay: 500
},
{
type: 'Enemy',
position: [360, 0],
name: 'Stage1Type13_2',
delay: 500
},
{
type: 'Enemy',
position: [640, -40],
name: 'Stage1Type13_2',
delay: 500
},
{
type: 'Enemy',
position: [820, -100],
name: 'Stage1Type13_2',
delay: 300
},
{
type: 'Enemy',
position: [820, -100],
name: 'Stage1Type13_2',
delay: 300
},
{
type: 'Enemy',
position: [200, 300],
name: 'Stage1Type14',
delay: 8000
},
{
type: 'Enemy',
position: [1080, 340],
name: 'Stage1Type14_2',
delay: 800
},
{
type: 'Enemy',
position: [200, 340],
name: 'Stage1Type14',
delay: 800
},
{
type: 'Enemy',
position: [1080, 300],
name: 'Stage1Type14_2',
delay: 800
},
{
type: 'Enemy',
position: [200, 300],
name: 'Stage1Type14',
delay: 100
},
{
type: 'Enemy',
position: [1080, 340],
name: 'Stage1Type14_2',
delay: 800
},
{
type: 'Enemy',
position: [200, 340],
name: 'Stage1Type14',
delay: 800
},
{
type: 'Enemy',
position: [1080, 300],
name: 'Stage1Type14_2',
delay: 800
},
{
type: 'Enemy',
position: [200, 300],
name: 'Stage1Type14',
delay: 100
},
{
type: 'Enemy',
position: [1080, 340],
name: 'Stage1Type14_2',
delay: 800
},
{
type: 'Enemy',
position: [200, 340],
name: 'Stage1Type14',
delay: 800
},
{
type: 'Enemy',
position: [1080, 300],
name: 'Stage1Type14_2',
delay: 800
},
{
type: 'Enemy',
position: [200, 300],
name: 'Stage1Type14',
delay: 100
},
{
type: 'Enemy',
position: [1080, 340],
name: 'Stage1Type14_2',
delay: 800
},
{
type: 'Enemy',
position: [200, 340],
name: 'Stage1Type14',
delay: 800
},
{
type: 'Enemy',
position: [1080, 300],
name: 'Stage1Type14_2',
delay: 800
},{
type: 'Enemy',
position: [200, 300],
name: 'Stage1Type14',
delay: 100
},
{
type: 'Enemy',
position: [1080, 340],
name: 'Stage1Type14_2',
delay: 800
},
{
type: 'Enemy',
position: [200, 340],
name: 'Stage1Type14',
delay: 800
},
{
type: 'Enemy',
position: [1080, 300],
name: 'Stage1Type14_2',
delay: 800
},
{
type: 'Enemy',
position: [200, 300],
name: 'Stage1Type14',
delay: 100
},
{
type: 'Enemy',
position: [1080, 340],
name: 'Stage1Type14_2',
delay: 800
},
{
type: 'Enemy',
position: [200, 340],
name: 'Stage1Type14',
delay: 800
},
{
type: 'Enemy',
position: [1080, 300],
name: 'Stage1Type14_2',
delay: 800
},
{
type: 'Enemy',
position: [200, 300],
name: 'Stage1Type14',
delay: 100
},
{
type: 'Enemy',
position: [1080, 340],
name: 'Stage1Type14_2',
delay: 800
},
{
type: 'Enemy',
position: [200, 340],
name: 'Stage1Type14',
delay: 800
},
{
type: 'Enemy',
position: [1080, 300],
name: 'Stage1Type14_2',
delay: 800
},
{
type: 'Enemy',
position: [200, 300],
name: 'Stage1Type14',
delay: 100
},
{
type: 'Enemy',
position: [1080, 340],
name: 'Stage1Type14_2',
delay: 800
},
{
type: 'Enemy',
position: [200, 340],
name: 'Stage1Type14',
delay: 800
},
{
type: 'Enemy',
position: [1080, 300],
name: 'Stage1Type14_2',
delay: 800
},{
type: 'Enemy',
position: [200, 300],
name: 'Stage1Type14',
delay: 100
},
{
type: 'Enemy',
position: [1080, 340],
name: 'Stage1Type14_2',
delay: 800
},
{
type: 'Enemy',
position: [200, 340],
name: 'Stage1Type14',
delay: 800
},
{
type: 'Enemy',
position: [1080, 300],
name: 'Stage1Type14_2',
delay: 800
},
{
type: 'Enemy',
position: [200, 300],
name: 'Stage1Type14',
delay: 100
},
{
type: 'Enemy',
position: [1080, 340],
name: 'Stage1Type14_2',
delay: 800
},
{
type: 'Enemy',
position: [200, 340],
name: 'Stage1Type14',
delay: 800
},
{
type: 'Enemy',
position: [1080, 300],
name: 'Stage1Type14_2',
delay: 800
},
{
type: 'Enemy',
position: [200, 300],
name: 'Stage1Type14',
delay: 100
},
{
type: 'Enemy',
position: [1080, 340],
name: 'Stage1Type14_2',
delay: 800
},
{
type: 'Enemy',
position: [200, 340],
name: 'Stage1Type14',
delay: 800
},
{
type: 'Enemy',
position: [1080, 300],
name: 'Stage1Type14_2',
delay: 800
},
{
type: 'Enemy',
position: [200, 300],
name: 'Stage1Type14',
delay: 100
},
{
type: 'Enemy',
position: [1080, 340],
name: 'Stage1Type14_2',
delay: 800
},
{
type: 'Enemy',
position: [200, 340],
name: 'Stage1Type14',
delay: 800
},
{
type: 'Enemy',
position: [1080, 300],
name: 'Stage1Type14_2',
delay: 800
},
{
type: 'Enemy',
position: [200, 400],
name: 'Stage1Type15',
delay: 12000
},
{
type: 'Enemy',
position: [200, 400],
name: 'Stage1Type15',
delay: 1000
},
{
type: 'Enemy',
position: [200, 400],
name: 'Stage1Type15',
delay: 1000
},
{
type: 'Enemy',
position: [200, 400],
name: 'Stage1Type15',
delay: 1000
},
{
type: 'Enemy',
position: [200, 400],
name: 'Stage1Type15',
delay: 1000
},
{
type: 'Enemy',
position: [200, 400],
name: 'Stage1Type15',
delay: 1000
},
{
type: 'Enemy',
position: [200, 400],
name: 'Stage1Type15',
delay: 1000
},
{
type: 'Enemy',
position: [200, 400],
name: 'Stage1Type15',
delay: 1000
},
{
type: 'Enemy',
position: [200, -100],
name: 'Stage1Type16',
delay: 1500
},
{
type: 'Enemy',
position: [1080, -100],
name: 'Stage1Type16_2',
delay: 500
},
{
type: 'Enemy',
position: [200, -100],
name: 'Stage1Type16',
delay: 500
},
{
type: 'Enemy',
position: [1080, -100],
name: 'Stage1Type16_2',
delay: 500
},
{
type: 'Enemy',
position: [200, -100],
name: 'Stage1Type16',
delay: 500
},
{
type: 'Enemy',
position: [1080, -100],
name: 'Stage1Type16_2',
delay: 500
},
{
type: 'Enemy',
position: [200, -100],
name: 'Stage1Type16',
delay: 500
},
{
type: 'Enemy',
position: [1080, -100],
name: 'Stage1Type16_2',
delay: 500
},
{
type: 'Enemy',
position: [200, 400],
name: 'Stage1Type15',
delay: 1000
},
{
type: 'Enemy',
position: [200, 400],
name: 'Stage1Type15',
delay: 1000
},
{
type: 'Enemy',
position: [200, 400],
name: 'Stage1Type15',
delay: 1000
},
{
type: 'Enemy',
position: [200, 400],
name: 'Stage1Type15',
delay: 1000
},
{
type: 'Enemy',
position: [200, 400],
name: 'Stage1Type15',
delay: 1000
},
{
type: 'Enemy',
position: [200, 400],
name: 'Stage1Type15',
delay: 1000
},
{
type: 'Enemy',
position: [200, 400],
name: 'Stage1Type15',
delay: 1000
},
{
type: 'Enemy',
position: [200, 400],
name: 'Stage1Type15',
delay: 1000
},
{
type: 'Enemy',
position: [200, 600],
name: 'Stage1Type16_3',
delay: 1500
},
{
type: 'Enemy',
position: [1080, 600],
name: 'Stage1Type16_4',
delay: 500
},
{
type: 'Enemy',
position: [200, 500],
name: 'Stage1Type16_3',
delay: 500
},
{
type: 'Enemy',
position: [1080, 500],
name: 'Stage1Type16_4',
delay: 500
},
{
type: 'Enemy',
position: [200, 600],
name: 'Stage1Type16_3',
delay: 500
},
{
type: 'Enemy',
position: [1080, 600],
name: 'Stage1Type16_4',
delay: 500
},
{
type: 'Enemy',
position: [200, 500],
name: 'Stage1Type16_3',
delay: 500
},
{
type: 'Enemy',
position: [1080, 500],
name: 'Stage1Type16_4',
delay: 500
},
{
type: 'Enemy',
position: [1080, 50],
name: 'Stage1Type17',
delay: 2000
},
{
type: 'Enemy',
position: [1080, 50],
name: 'Stage1Type17',
delay: 500
},
{
type: 'Enemy',
position: [1080, 50],
name: 'Stage1Type17',
delay: 500
},
{
type: 'Enemy',
position: [1080, 50],
name: 'Stage1Type17',
delay: 500
},{
type: 'Enemy',
position: [1080, 50],
name: 'Stage1Type17',
delay: 500
},
{
type: 'Enemy',
position: [1080, 50],
name: 'Stage1Type17',
delay: 500
},
{
type: 'Enemy',
position: [1080, 50],
name: 'Stage1Type17',
delay: 500
},
{
type: 'Enemy',
position: [1080, 50],
name: 'Stage1Type17',
delay: 500
},
{
type: 'Enemy',
position: [1080, 50],
name: 'Stage1Type17',
delay: 500
},
{
type: 'Enemy',
position: [1080, 50],
name: 'Stage1Type17',
delay: 500
},
{
type: 'Enemy',
position: [1080, 50],
name: 'Stage1Type17',
delay: 500
},
{
type: 'Enemy',
position: [1080, 50],
name: 'Stage1Type17',
delay: 500
},
{
type: 'Enemy',
position: [1080, 50],
name: 'Stage1Type17',
delay: 500
},
{
type: 'Enemy',
position: [1080, 50],
name: 'Stage1Type17',
delay: 500
},
{
type: 'Enemy',
position: [1080, 50],
name: 'Stage1Type17',
delay: 500
},
{
type: 'Enemy',
position: [1080, 50],
name: 'Stage1Type17',
delay: 500
},
{
type: 'Enemy',
position: [1080, 50],
name: 'Stage1Type17',
delay: 500
},
{
type: 'Enemy',
position: [1080, 50],
name: 'Stage1Type17',
delay: 500
},
{
type: 'Enemy',
position: [1080, 50],
name: 'Stage1Type17',
delay: 500
},
{
type: 'Enemy',
position: [1080, 50],
name: 'Stage1Type17',
delay: 500
},

{
type: 'Enemy',
position: [200, 50],
name: 'Stage1Type17_2',
delay: 2000
},
{
type: 'Enemy',
position: [200, 50],
name: 'Stage1Type17_2',
delay: 500
},
{
type: 'Enemy',
position: [200, 50],
name: 'Stage1Type17_2',
delay: 500
},
{
type: 'Enemy',
position: [200, 50],
name: 'Stage1Type17_2',
delay: 500
},{
type: 'Enemy',
position: [200, 50],
name: 'Stage1Type17_2',
delay: 500
},
{
type: 'Enemy',
position: [200, 50],
name: 'Stage1Type17_2',
delay: 500
},
{
type: 'Enemy',
position: [200, 50],
name: 'Stage1Type17_2',
delay: 500
},
{
type: 'Enemy',
position: [200, 50],
name: 'Stage1Type17_2',
delay: 500
},
{
type: 'Enemy',
position: [200, 50],
name: 'Stage1Type17_2',
delay: 500
},
{
type: 'Enemy',
position: [200, 50],
name: 'Stage1Type17_2',
delay: 500
},
{
type: 'Enemy',
position: [200, 50],
name: 'Stage1Type17_2',
delay: 500
},
{
type: 'Enemy',
position: [200, 50],
name: 'Stage1Type17_2',
delay: 500
},
{
type: 'Enemy',
position: [200, 50],
name: 'Stage1Type17_2',
delay: 500
},
{
type: 'Enemy',
position: [200, 50],
name: 'Stage1Type17_2',
delay: 500
},
{
type: 'Enemy',
position: [200, 50],
name: 'Stage1Type17_2',
delay: 500
},
{
type: 'Enemy',
position: [200, 50],
name: 'Stage1Type17_2',
delay: 500
},
{
type: 'Enemy',
position: [200, 50],
name: 'Stage1Type17_2',
delay: 500
},
{
type: 'Enemy',
position: [200, 50],
name: 'Stage1Type17_2',
delay: 500
},
{
type: 'Enemy',
position: [200, 50],
name: 'Stage1Type17_2',
delay: 500
},
{
type: 'Enemy',
position: [200, 50],
name: 'Stage1Type17_2',
delay: 500
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_3',
delay: 3000
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_4',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_3',
delay: 800
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_4',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_3',
delay: 800
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_4',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_3',
delay: 800
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_4',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_3',
delay: 800
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_4',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_3',
delay: 800
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_4',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_3',
delay: 800
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_4',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_3',
delay: 800
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_4',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_3',
delay: 800
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_4',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_3',
delay: 800
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_4',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_3',
delay: 800
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_4',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_3',
delay: 800
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_4',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_3',
delay: 800
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_4',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_5',
delay: 3000
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_6',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_5',
delay: 600
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_6',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_5',
delay: 600
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_6',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_5',
delay: 600
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_6',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_5',
delay: 600
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_6',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_5',
delay: 600
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_6',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_5',
delay: 600
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_6',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_5',
delay: 600
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_6',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_5',
delay: 600
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_6',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_5',
delay: 600
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_6',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_5',
delay: 600
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_6',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_5',
delay: 600
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_6',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_5',
delay: 600
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_6',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_5',
delay: 600
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_6',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_7',
delay: 3000
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_8',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_7',
delay: 600
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_8',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_7',
delay: 600
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_8',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_7',
delay: 600
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_8',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_7',
delay: 600
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_8',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_7',
delay: 600
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_8',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_7',
delay: 600
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_8',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_7',
delay: 600
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_8',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_7',
delay: 600
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_8',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_7',
delay: 600
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_8',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_7',
delay: 600
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_8',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_7',
delay: 600
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_8',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_7',
delay: 600
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_8',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_7',
delay: 600
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_8',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_7',
delay: 600
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_8',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_7',
delay: 600
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_8',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_7',
delay: 600
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_8',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_7',
delay: 600
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_8',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_7',
delay: 600
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_8',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_7',
delay: 600
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_8',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_7',
delay: 600
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_8',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_7',
delay: 600
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_8',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_7',
delay: 600
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_8',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_7',
delay: 600
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_8',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_7',
delay: 600
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_8',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_7',
delay: 600
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_8',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_7',
delay: 600
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_8',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_7',
delay: 600
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_8',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_7',
delay: 600
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_8',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_7',
delay: 600
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_8',
delay: 0
},
{
type: 'Enemy',
position: [1080, 100],
name: 'Stage1Type17_7',
delay: 600
},
{
type: 'Enemy',
position: [200, 100],
name: 'Stage1Type17_8',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 8000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 500
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 500
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 500
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [560, -10],
name: 'Stage1Type18',
delay: 1000
},
{
type: 'Enemy',
position: [720, -10],
name: 'Stage1Type18_2',
delay: 0
},
{
type: 'Enemy',
position: [640, 0],
name: 'Stage1Type19',
delay: 12000
},
{
type: 'Enemy',
position: [640, 0],
name: 'Stage1Type19_2',
delay: 18000
},
]";
    }
}
