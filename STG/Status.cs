﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STG
{
    static class Status
    {
        public static int Lives { get; private set; }
        public static int Bombs { get; private set; }
        public static int Score { get; private set; }
        public static float Multiplier { get; private set; }

        static int power;

        public static int Power
        {
            get
            {
                return power;
            }

            private set
            {
                if (value < 0)
                    power = 0;
                else if (value > 100)
                    power = 100;
                else
                    power = value;
            }
        }

        static bool over1 = false;
        static bool over2 = false;
        static bool over3 = false;
        static bool over4 = false;

        public static bool IsGameOver { get { return Lives < 0; } }
        public static bool IsGameCleared = false;

        public static bool IsBossApproaching = false;

        static Status()
        {
            Reset();
        }

        public static void Reset()
        {
            Score = 0;
            Lives = 2;
            Bombs = 3;
            Multiplier = 1f;
            Power = 0;
            IsGameCleared = false;
        }

        public static void AddScore(int point)
        {
            Score += (int)(point * Multiplier);

            if (Score >= 100000 && !over1)
            {
                over1 = true;
                AddLife();
            }

            if (Score >= 500000 && !over2)
            {
                over2 = true;
                AddLife();
            }

            if (Score >= 2000000 && !over3)
            {
                over3 = true;
                AddLife();
            }

            if (Score >= 5000000 && !over4)
            {
                over4 = true;
                AddLife();
            }
        }

        public static void AddLife()
        {
            Content.Audio.PlayerGainsLife.Play(1f, 0f, 0f);
            Lives++;
        }


        public static void AddBomb()
        {
            Bombs++;
        }

        public static void AddPower()
        {
            Power++;
        }

        public static void AddMultiplier()
        {
            Multiplier += .01f;
        }

        public static void RemoveLife()
        {
            Lives--;
        }

        public static void RemoveBomb()
        {
            Bombs--;
        }

        public static void RemovePower()
        {
            Power -= 20;
        }

        public static void ResetBomb()
        {
            if (Bombs < 3)
                Bombs = 3;
        }

        public static void ResetMultiplier()
        {
            Multiplier = 1f;
        }
    }
}
