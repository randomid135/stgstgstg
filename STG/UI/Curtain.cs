﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STG.UI
{
    class Curtain
    {
        public void Draw(SpriteBatch spriteBatch, int alpha)
        {
            spriteBatch.Draw(Content.Sprite.TitleMenuWrapper, Vector2.Zero, new Rectangle(0, 0, (int)Main.ScreenSize.X, (int)Main.ScreenSize.Y), new Color(0, 0, 0, alpha));
        }
    }
}
