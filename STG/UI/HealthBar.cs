﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STG.UI
{
    class HealthBar : UI
    {
        readonly int health;
        int currentHealth;
        Rectangle healthRect, currentHealthRect;

        public HealthBar(int health)
        {
            this.health = health;     

            Initialize();
        }

        public int CurrentHealth
        {
            get
            {
                return currentHealth;
            }
            set
            {
                if (value >= 0 && value <= health)
                {
                    currentHealth = value;
                    Update();
                }
                    
            }
        }

        protected override void Initialize()
        {
            healthRect = new Rectangle(330, 20, 600, 10);
            currentHealthRect = healthRect;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Content.Sprite.HealthBarInner, healthRect, new Color(128, 128, 128, 128));
            spriteBatch.Draw(Content.Sprite.HealthBarInner, currentHealthRect, new Color(255, 255, 255, 128));
        }

        protected override void Update()
        {
            float percentage = (float)CurrentHealth / health;
            currentHealthRect.Width = (int)(percentage * healthRect.Width);
        }
    }
}
