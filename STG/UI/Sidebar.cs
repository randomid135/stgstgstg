﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace STG.UI
{
    class Sidebar
    {
        public void Draw(SpriteBatch spriteBatch)
        {
            var lives = Status.Lives < 0 ? 0 : Status.Lives;

            spriteBatch.Draw(Content.Sprite.PlayingSideBar, Vector2.Zero, null, Color.White);

            spriteBatch.DrawString(Content.Sprite.MainFont, Status.Score.ToString().PadLeft(10, '0'), new Vector2(50, 50), Color.White, 0f, Vector2.Zero, .5f, SpriteEffects.None, Constant.LayerDepth.SideBarText);
            spriteBatch.DrawString(Content.Sprite.MainFont, "x " + Status.Multiplier.ToString("0.00"), new Vector2(50, 100), Color.White, 0f, Vector2.Zero, .5f, SpriteEffects.None, Constant.LayerDepth.SideBarText);
            spriteBatch.DrawString(Content.Sprite.MainFont, "Lives: " + lives, new Vector2(50, 400), Color.White, 0f, Vector2.Zero, .5f, SpriteEffects.None, Constant.LayerDepth.SideBarText);
            spriteBatch.DrawString(Content.Sprite.MainFont, "Bombs: " + Status.Bombs, new Vector2(50, 450), Color.White, 0f, Vector2.Zero, .5f, SpriteEffects.None, Constant.LayerDepth.SideBarText);
            //spriteBatch.DrawStringWithStroke(Content.Sprite.MainFont, "Power: " + Status.Power, new Vector2(25, 250), Color.White, Color.Black, 1f);
        }
    }
}
