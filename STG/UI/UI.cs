﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace STG.UI
{
    public abstract class UI
    {
        abstract protected void Initialize();

        abstract public void Draw(SpriteBatch spriteBatch);

        abstract protected void Update();
    }
}
